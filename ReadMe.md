Keystore details:
=================

- all passwords are : lolchamps
- Alias is: LolChamps for live version - keystore name is : keystore_of_current_app_on_play_store.jks - package : com.hisham.lolchamps

OLDER VERSION - IGNORE - Deprecated.
======================
- Alias is: LolChamps for 1st version - keystore name is : keystore.jks - package : com.hisham.lolchamps
- Alias is: GuideForLol for 2nd version - keystore name is : keystoreLol.jks - package : com.hisham.lolguide

Latest version of data dragon api: 
==================================

- Url to get latest data dragon champ version: https://ddragon.leagueoflegends.com/api/versions.json
- Current latest data version is fetched from Firebase API: https://console.firebase.google.com/u/0/project/lolchamps-11338/config - This should soon be deprecated.
- Link to get summary details of all champs: http://ddragon.leagueoflegends.com/cdn/10.5.1/data/en_US/champion.json
- For more details about the API : https://riot-api-libraries.readthedocs.io/en/latest/ddragon.html

Deprecated: 
-----------

 - https://developer.riotgames.com/static-data.html

Resources details:
============

Champions (Splash Art):
http://ddragon.leagueoflegends.com/cdn/img/champion/splash/Aatrox_0.jpg

Champions (Loading Screen Art):
http://ddragon.leagueoflegends.com/cdn/img/champion/loading/Aatrox_0.jpg

The number at the end of the image follows the same convention described in Champions (Splash Art).

Champions (Square):
http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/Aatrox.png

League Wallpaper details:
==============

- Full json link, size 1mb : http://screensavers.riotgames.com/v2/latest/content/data.json
- Thumbnail : http://screensavers.riotgames.com/v2/latest/content/thumbnail/AnimatedArt/animated-pyke.jpg
- Full image: http://screensavers.riotgames.com/v2/latest/content/original/ChampionIllustrations/GroupSplashes/2018-poolparty-group-splash.jpg
- Video url link webm file animated: http://screensavers.riotgames.com/v2/latest/content/original/AnimatedArt/classic-zed-loop.webm

How about this:
https://www.sporcle.com/games/connieaitken31/league-of-legends-whos-that-champion