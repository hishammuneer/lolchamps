package com.hisham.lolchamps

import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdView
import com.google.android.material.tabs.TabLayout
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.adapters.ChampDetailsAdapter
import com.hisham.lolchamps.fragments.*
import com.hisham.lolchamps.helper.AdHelper
import net.rithms.riot.api.endpoints.static_data.dto.Champion

class ChampDetailActivity : AppCompatActivity(), SkinsFragment.OnFragmentInteractionListener,
    TipsFragment.OnFragmentInteractionListener, LoreFragment.OnFragmentInteractionListener,
    SkillsFragment.OnFragmentInteractionListener,
    LoadingSkinsFragment.OnFragmentInteractionListener {
    private var tabs: TabLayout? = null

    // JazzyViewPager pagers;
    private var pager: JazzyViewPager? = null
    private var adViewChamp: AdView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_champ_detail)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tabs = findViewById<View>(R.id.tabs) as TabLayout
        pager = findViewById<View>(R.id.pager) as JazzyViewPager
        adViewChamp = findViewById<View>(R.id.adViewChamp) as AdView
        val champion: Champion?
        val bundle = intent.extras
        champion = if (bundle != null && bundle.containsKey("champion")) {
            bundle.getSerializable("champion") as Champion?
        } else {
            Toast.makeText(this, "No data found.", Toast.LENGTH_SHORT).show()
            return
        }
        if (champion != null) {
            title = champion.name
        }
        val ChampOverViewAdapter = ChampDetailsAdapter(
            supportFragmentManager, pager!!, champion!!
        )
        pager!!.adapter = ChampOverViewAdapter

        // change the title of the main activity
//        setTitle(((RadioButton) view).getText().toString());
        if (pager != null && tabs != null) {
            val pageMargin = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4f, resources
                    .displayMetrics
            ).toInt()
            pager!!.pageMargin = pageMargin
            tabs!!.tabMode = TabLayout.GRAVITY_CENTER
            tabs!!.setupWithViewPager(pager)
        }
        loadAd()
    }

    private fun loadAd() {
        if (Globals.isOnline(this)) {
            adViewChamp!!.loadAd(AdHelper(this).adRequest)
            adViewChamp!!.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adViewChamp!!.visibility = View.VISIBLE
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    adViewChamp!!.visibility = View.GONE
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFragmentInteraction(uri: Uri?) {}
}