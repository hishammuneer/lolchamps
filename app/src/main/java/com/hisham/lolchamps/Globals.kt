package com.hisham.lolchamps

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.text.TextUtils
import android.widget.ImageView
import coil.load
import java.io.IOException
import java.nio.charset.Charset
import java.text.DecimalFormat

/**
 * Created by Hisham on 15/Mar/2017 - 14:19
 */
object Globals {
    var adWatchedPerAppLifecycle = false
    fun setTagImage(tag: String, imageView: ImageView) {
        if (!TextUtils.isEmpty(tag)) {
            if (tag.equals("Assassin", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.assasin)
            } else if (tag.equals("Fighter", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.fighter)
            } else if (tag.equals("Marksman", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.marksman)
            } else if (tag.equals("Mage", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.mage)
            } else if (tag.equals("Support", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.support)
            } else if (tag.equals("Tank", ignoreCase = true)) {
                imageView.setImageResource(R.drawable.tank)
            }
        }
    }

    fun loadRegionsJSONFromAsset(activity: Activity): String? {
        val json: String
        json = try {
            val `is` = activity.assets.open("regions.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, Charset.defaultCharset())
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun isOnline(activity: Activity): Boolean {
        val cm = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun getFileSize(size: Long): String {
        if (size <= 0) return "0"
        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(
            size / Math.pow(
                1024.0,
                digitGroups.toDouble()
            )
        ) + " " + units[digitGroups]
    }

    fun getButtonCharBasedOnPosition(i: Int): String {
        return when (i) {
            0 -> " (Q) "
            1 -> " (W) "
            2 -> " (E) "
            3 -> " (R) "
            else -> ""
        }
    }

    /**
     * Determine if the device is a tablet (i.e. it has a large screen).
     *
     * @param context The calling context.
     */
    fun isTablet(context: Context): Boolean {
        return ((context.resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }

    private val TAG = Globals::class.java.simpleName
    fun loadImageWithInvisibility(
        imgUrl: String,
        imgBack: ImageView
    ) {
        imgBack.load(imgUrl) {
            crossfade(true)
            placeholder(R.drawable.place_holder)
        }
    }

    fun loadImage(imgUrl: String, imgBack: ImageView) {
        loadImageWithInvisibility(imgUrl, imgBack)
    }
}