package com.hisham.lolchamps

import android.app.Activity
import android.app.AlertDialog
import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager
import coil.imageLoader
import coil.request.ImageRequest
import com.google.android.gms.ads.reward.RewardItem
import com.hisham.lolchamps.adapters.SlidingImageAdapter
import com.hisham.lolchamps.adapters.SlidingImageAdapter.SlidingAdapterListener
import com.hisham.lolchamps.helper.*
import com.hisham.lolchamps.helper.AdHelper.RewardAdHelperListener
import com.hisham.lolchamps.helper.DownloadHelper.download
import com.hisham.lolchamps.model.WallContainerModel
import com.hisham.lolchamps.model.WallItemModel
import com.hisham.lolchamps.utils.io
import com.hisham.lolchamps.utils.main
import com.soundcloud.android.crop.Crop
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.*

/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Must be called with 1 bundle extra, imageLink - link of the image you want to display
 * another optional bundle extra is title.
 */
class ImageActivity : AppCompatActivity() {
    //    private TextView mCurrMatrixTv;
    private var adHelper: AdHelper? = null
    private var viewPager: ViewPager? = null
    private var slidingImageAdapter: SlidingImageAdapter? = null
    private var listOfImages: MutableList<WallItemModel?>? = null
    private var inputUri: Uri? = null
    private var outputUri: Uri? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        viewPager = findViewById<View>(R.id.pager) as ViewPager
        adHelper = AdHelper(applicationContext).setUpRewardAd()
        adHelper?.setRewardAdListener(object : RewardAdHelperListener {
            override fun onRewardAdClosed() {}
            override fun onRewarded(reward: RewardItem?) {
                Globals.adWatchedPerAppLifecycle = true
                Toast.makeText(
                    this@ImageActivity,
                    Html.fromHtml("Thank you for the support. :)"),
                    Toast.LENGTH_LONG
                ).show()
            }
        })
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }
        val bundle = intent.extras
        if (bundle != null) {
            listOfImages = null
            val imageLink = bundle.getString("imageLink")
            val containerModel = bundle.getSerializable("wallContainer") as WallContainerModel?
            val currentItem = bundle.getInt("currentItem", -1)
            val title = bundle.getString("title", null)
            if (!TextUtils.isEmpty(title)) {
                setTitle(title)
            }
            if (!TextUtils.isEmpty(imageLink)) {
                listOfImages = ArrayList()
                val model = WallItemModel().setLink(imageLink).setTitle(title)
                listOfImages?.add(model)
            } else if (containerModel != null && containerModel.data?.size ?: 0 > 0) {
                listOfImages = ArrayList(containerModel.data)
            }
            slidingImageAdapter = listOfImages?.let {
                SlidingImageAdapter(
                    this@ImageActivity,
                    it,
                    currentItem,
                    object : SlidingAdapterListener {
                        override fun onSetWallClicked(url: String?) {
                            wallpaperSetEvent(url)
                        }

                        override fun onDownloadHDClicked(url: String?) {
                            downloadImage(url)
                        }
                    })
            }
            viewPager?.adapter = slidingImageAdapter
            if (currentItem != -1) {
                viewPager?.currentItem = currentItem
            }
        }
    }

    private fun wallpaperSetEvent(url: String?) {
        // ad
        if (adHelper?.isRewardAdLoaded == true && !Globals.adWatchedPerAppLifecycle) {
            val builder = AlertDialog.Builder(this@ImageActivity)
            val message = "Watch a video Ad to support the developer?"
            builder.setTitle("Support Us").setMessage(message)
            builder.setPositiveButton("Yes, I'd love to Help.") { dialogInterface, i ->
                setWall(
                    true,
                    url
                )
            }
            builder.setNegativeButton("No, I Don't Care") { dialogInterface, i ->
                setWall(
                    false,
                    url
                )
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()
        } else {
            setWall(false, url)
        }
    }

    private fun setWall(showAdFirst: Boolean, url: String?) {
        if (showAdFirst) {
            adHelper?.showRewardAd()
        }
        setAsBG(this, url)
    }

    private fun setAsBG(activity: Activity, url: String?) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Set image as wallpaper")
            .setMessage("Do you want to crop this image?")
            .setPositiveButton("Crop") { dialog, which ->
                downloadImage(true, url)
            }
            .setNegativeButton(
                "Use Full"
            ) { dialog, which ->
                downloadImage(false, url)
            }
        val dialog = builder.create()
        dialog.show()
    }

    private fun downloadImage(cropFirst: Boolean, url: String?) {

        val request = ImageRequest.Builder(this)
            .data(url)
            .build()

        lifecycleScope.launch {
            io {
                val toBitmap = imageLoader.execute(request).drawable?.toBitmap()
                toBitmap?.let {
                    if (!cropFirst) {
                        finallySetWallPaper(it)
                    } else {
                        inputUri = getImageUri(this@ImageActivity, it)
                        val file = File(cacheDir, "/cropped.jpg")
                        if (!file.exists()) {
                            @Suppress("BlockingMethodInNonBlockingContext")
                            file.createNewFile()
                        }
                        outputUri = Uri.fromFile(file)
                        Crop.of(inputUri, outputUri).start(this@ImageActivity)
                    }
                } ?: kotlin.run {
                    // if bitmap is not received
                    main {
                        Toast.makeText(
                            this@ImageActivity,
                            "Something went wrong",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, result: Intent?) {
        super.onActivityResult(requestCode, resultCode, result)
        if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, outputUri)
                finallySetWallPaper(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
                Toast.makeText(this, "Error setting wallpaper", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "temp", null)
        return Uri.parse(path)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_skin, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val currentItemPosition = viewPager?.currentItem ?: 0
        return when (item.itemId) {
            R.id.menu_wallpaper -> {
                if (listOfImages != null && listOfImages?.size != 0 &&
                    listOfImages?.size ?: 0 >= currentItemPosition
                ) {
                    wallpaperSetEvent(listOfImages?.get(currentItemPosition)?.link)
                }
                true
            }
            android.R.id.home -> {
                finish()
                true
            }
            R.id.menu_download -> {
                if (listOfImages != null && listOfImages?.size != 0 &&
                    listOfImages?.size ?: 0 >= currentItemPosition
                ) {
                    downloadImage(listOfImages?.get(currentItemPosition)?.link)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun downloadImage(url: String?) {
        if (!TextUtils.isEmpty(url)) {
            PermissionHelper().requestPermission(
                this,
                object : PermissionHelperListener {
                    override fun onPermission(granted: Boolean) {
                        if (granted) {
                            url?.trim { it <= ' ' }?.let {
                                download(this@ImageActivity, it, null)
                            }
                            Toast.makeText(
                                this@ImageActivity,
                                R.string.downloading_check_notifications,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                },
                viewPager
            )
        }
    }

    private fun finallySetWallPaper(theBitmap: Bitmap) {
        val myWallpaperManager = WallpaperManager.getInstance(this)
        myWallpaperManager.setBitmap(theBitmap)
        lifecycleScope.launch(Dispatchers.Main) {
            Toast.makeText(this@ImageActivity, "Wallpaper set.", Toast.LENGTH_SHORT).show()
        }
    }
}