package com.hisham.lolchamps

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.app.SearchManager
import android.content.ActivityNotFoundException
import android.content.Intent
import android.database.MatrixCursor
import android.net.Uri
import android.os.Bundle
import android.provider.BaseColumns
import android.text.Html
import android.text.TextUtils
import android.util.TypedValue
import android.view.*
import android.widget.CursorAdapter
import android.widget.SearchView
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.adapters.ChampOverViewAdapter
import com.hisham.lolchamps.adapters.RegionsAdapter
import com.hisham.lolchamps.adapters.WallAdapter
import com.hisham.lolchamps.databinding.ActivityIntroBinding
import com.hisham.lolchamps.fragments.LoreFragment
import com.hisham.lolchamps.fragments.TipsFragment
import com.hisham.lolchamps.fragments.WallRecentsFragment.OnWallRecentFragmentInteractionListener
import com.hisham.lolchamps.helper.AdHelper
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.helper.PrivacyHelper.Companion.getPrivacyPolicy
import com.hisham.lolchamps.interfaces.IChampion
import com.hisham.lolchamps.server.ServerHit
import com.hisham.lolchamps.viewmodels.MainActivityViewModel
import com.hisham.permissionshelper.IPermission
import com.hisham.permissionshelper.IPermissionCallback
import com.hisham.permissionshelper.PermissionImplementation
import net.rithms.riot.api.endpoints.static_data.dto.Champion
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    TipsFragment.OnFragmentInteractionListener,
    LoreFragment.OnFragmentInteractionListener,
    OnWallRecentFragmentInteractionListener {

    // to find the last postion of the user
    private var prefs: AppSharedPrefs? = null
    private var lastSelection: Int? = 0
    private var adView: AdView? = null
    private var pager: JazzyViewPager? = null
    private var tabs: TabLayout? = null
    private var uiTopLL: ViewGroup? = null
    private var wallAdapter: WallAdapter? = null
    private var searchView: SearchView? = null
    private var progressDialog: ProgressDialog? = null

    // view binding
    private lateinit var binding: ActivityIntroBinding
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        progressDialog = ProgressDialog(this)
        progressDialog?.setMessage("Loading. Please wait...")
        progressDialog?.isIndeterminate = true
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog?.setCancelable(true)
        progressDialog?.show()
        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]
        prefs = AppSharedPrefs.getInstance(this)

        viewModel.champList.observe(this) {
            championList = it
            // Sharedpreferences to save the state of which button was active
            // if prefs not found set the first radio button checked, call the
            // onclick method with first radio button view
            // and then close the drawer
            // else save the view id, find the view by its id, pass the view to
            // onclick and close the drawer

            lastSelection = prefs?.lastSelection
            if (lastSelection == -1) {
                lastSelection = 2 // 3rd position is default, wallpaper one
            }
            onSideNavSelected(lastSelection!!)
            progressDialog?.dismiss()
        }
        viewModel.getChamps(prefs!!)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        uiTopLL = findViewById<View>(R.id.uiTopLL) as ViewGroup
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713") // todo fix this
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)
        binding.navView.setCheckedItem(R.id.nav_wall) // checking the first item
        adView = findViewById<View>(R.id.adView) as AdView
        pager = findViewById<View>(R.id.container) as JazzyViewPager
        tabs = findViewById<View>(R.id.tabs) as TabLayout

//        ServerHit.getInstance().getChampions(this, new IChampions() {
//            @Override
//            public void onChampionsFetched(ChampionList list) {
//                championList = list;
//
//                // Sharedpreferences to save the state of which button was active
//                // if prefs not found set the first radio button checked, call the
//                // onclick method with first radio button view
//                // and then close the drawer
//                // else save the view id, find the view by its id, pass the view to
//                // onclick and close the drawer
//                prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//
//                lastSelection = prefs.getInt("lastSelection", -1);
//                if (lastSelection == -1) {
//                    lastSelection = 2; // 3rd position is default, wallpaper one
//                }
//                onSideNavSelected(lastSelection);
//                progressDialog.dismiss();
//            }
//        });
        loadAd()
        val from = arrayOf("champions")
        val to = intArrayOf(android.R.id.text1)
        mAdapter = SimpleCursorAdapter(
            this,
            R.layout.row_search_item_layout,
            null,
            from,
            to,
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        if (!iPermission.isPermissionGranted(this@MainActivity, permission)) {
            iPermission.requestPermission(
                this@MainActivity,
                permission,
                1,
                object : IPermissionCallback {
                    override fun permissionGranted(requestCode: Int) {
                        Snackbar.make(uiTopLL!!, "Storage permission required to download images.", Snackbar.LENGTH_SHORT).show()
                    }

                    override fun permissionDenied(requestCode: Int, isDeniedPreviously: Boolean) {
                        Snackbar.make(
                            uiTopLL!!,
                            "Storage permission granted.",
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction("Open Settings") { iPermission.openPermissionSettings(this@MainActivity) }
                            .show()
                    }
                })
        }
    }

    var iPermission: IPermission = PermissionImplementation.getInstance()
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        iPermission.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun loadAd() {
        if (Globals.isOnline(this)) {
            adView!!.loadAd(AdHelper(this).adRequest)
            adView!!.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adView!!.visibility = View.VISIBLE
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    adView!!.visibility = View.GONE
                }
            }
        }
    }

    /**
     * @param text can be null
     */
    private fun shareApp() {
        var text = ""
        val applicationNameId = applicationInfo.labelRes
        val appPackageName = BuildConfig.APPLICATION_ID
        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_SUBJECT, getString(applicationNameId))
        if (TextUtils.isEmpty(text)) {
            text = getString(R.string.install_this_cool_application)
        }
        val link = "https://play.google.com/store/apps/details?id=$appPackageName"
        i.putExtra(Intent.EXTRA_TEXT, "$text $link")
        startActivity(Intent.createChooser(i, "Share link:"))
    }

    private fun onSideNavSelected(position: Int) {
        // for every new fragment you need a layout, an adapter, tabs and
        // pagers, set the adapter
        tabs?.visibility = View.VISIBLE
        when (position) {
            2 -> {
                wallAdapter = WallAdapter(supportFragmentManager, pager!!)
                pager?.adapter = wallAdapter
                binding.navView.setCheckedItem(R.id.nav_wall)
                tabs?.visibility = View.GONE
            }
            0 -> {
                championList?.let {
                    val ChampOverViewAdapter = ChampOverViewAdapter(
                        supportFragmentManager, pager!!, it
                    )
                    pager?.adapter = ChampOverViewAdapter
                    binding.navView.setCheckedItem(R.id.nav_champions)
                }
            }
            1 -> {
                val regionsAdapter = RegionsAdapter(
                    supportFragmentManager,
                    pager!!,
                    this
                )
                pager?.adapter = regionsAdapter
                binding.navView.setCheckedItem(R.id.nav_regions)
            }
            else -> {
                val ChampOverViewAdapter = championList?.let {
                    ChampOverViewAdapter(
                        supportFragmentManager, pager!!, it
                    )
                }
                pager?.adapter = ChampOverViewAdapter
            }
        }
        // switch case ends

        // change the title of the main activity
//        setTitle(((RadioButton) view).getText().toString());
        if (pager != null && tabs != null) {
            val pageMargin = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4f, resources
                    .displayMetrics
            ).toInt()
            pager!!.pageMargin = pageMargin
            tabs!!.tabMode = TabLayout.MODE_SCROLLABLE
            tabs!!.setupWithViewPager(pager)
        }
        // close the menu when radio button is clicked
//        mDrawer.closeMenu(true);
        // saving the last position
        prefs?.saveIntPreference("lastSelection", position)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.navigation, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView?.suggestionsAdapter = mAdapter
        //        searchView.setIconifiedByDefault(false);

        // Getting selected (clicked) item suggestion
        searchView?.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionClick(position: Int): Boolean {
                if (!wallFragRunning) onSearchSubmit(
                    mAdapter!!.cursor.getString(
                        mAdapter!!.cursor.getColumnIndex(
                            "champions"
                        )
                    )
                )
                return true
            }

            override fun onSuggestionSelect(position: Int): Boolean {
                if (!wallFragRunning) onSearchSubmit(
                    mAdapter!!.cursor.getString(
                        mAdapter!!.cursor.getColumnIndex(
                            "champions"
                        )
                    )
                )
                return true
            }
        })
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                if (!wallFragRunning) {
                    onSearchSubmit(s)
                }
                return true
            }

            override fun onQueryTextChange(s: String): Boolean {
                if (wallFragRunning) {
                    populateAdapter(null)
                    if (wallAdapter != null && wallAdapter?.wallFrag != null) {
                        val wallFragment = wallAdapter?.wallFrag
                        wallFragment?.sortList(s)
                    }
                } else {
                    populateAdapter(s)
                }
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun onSearchSubmit(championName: String) {
        if (!TextUtils.isEmpty(championName)) {
            val mKeys = championList?.data?.keys?.toTypedArray()
            if (mKeys != null) {
                for (mKey in mKeys) {
                    if (championList?.data?.get(mKey)?.name.equals(
                            championName,
                            ignoreCase = true
                        )
                    ) {
                        loadChampion(championList?.data?.get(mKey))
                        break
                    }
                }
            }
        }
    }

    private fun loadChampion(champion: Champion?) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading. Please wait...")
        progressDialog.isIndeterminate = false
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
        progressDialog.show()
        ServerHit.instance.getChampionByKey(this, champion!!.id, object : IChampion {
            override fun onChampionLoaded(champion: Champion?) {
                progressDialog.dismiss()
                if (champion == null) {
                    Toast.makeText(
                        this@MainActivity,
                        "Unable to load data, please try again later.",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                val intent = Intent(this@MainActivity, ChampDetailActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("champion", champion)
                startActivity(intent.putExtras(bundle))
            }
        })
    }

    private var mAdapter: SimpleCursorAdapter? = null

    // You must implements your logic to get data using OrmLite
    private fun populateAdapter(query: String?) {
        if (query == null) {
            mAdapter?.changeCursor(null)
            return
        }
        val c = MatrixCursor(arrayOf(BaseColumns._ID, "champions"))
        val mKeys = championList?.data?.keys?.toTypedArray()
        mKeys?.let {
            for (i in mKeys.indices) {
                if (championList?.data?.get(mKeys[i])?.name?.toLowerCase()
                        ?.startsWith(query.toLowerCase()) == true
                ) c.addRow(
                    championList?.data?.get(mKeys[i])?.name?.let {
                        arrayOf<Any>(
                            i, it
                        )
                    }
                )
            }
            mAdapter?.changeCursor(c)
        }
    }

    // clicking the home icon will toggle the drawer
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> //                mDrawer.toggleMenu();
                return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        if (id == R.id.nav_champions) {
            onSideNavSelected(0)
        } else if (id == R.id.nav_regions) {
            onSideNavSelected(1)
        } else if (id == R.id.nav_wall) {
            onSideNavSelected(2)
        } else if (id == R.id.nav_share) {
            shareApp()
        } else if (id == R.id.nav_feedback) {
            feedback()
        } else if (id == R.id.nav_rate_us) {
            goToMarketPlace()
        } else if (id == R.id.nav_about_us) {
            aboutApp()
        } else if (id == R.id.nav_privacy) {
            privacyPolicy()
        }
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun feedback() {
        val subject =
            "Feedback for " + getString(R.string.app_name) + " V." + BuildConfig.VERSION_NAME
        val mailTo = Uri.fromParts("mailto", "hisham.dbd@gmail.com", null)
        mailTo.buildUpon().appendQueryParameter("subject", subject)
        val emailIntent = Intent(Intent.ACTION_SENDTO, mailTo)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(Intent.createChooser(emailIntent, "Send email..."))
    }

    private fun privacyPolicy() {
        val builder = AlertDialog.Builder(this@MainActivity)
        val message = getPrivacyPolicy()
        builder.setTitle("Privacy Policy").setMessage(Html.fromHtml(message))
        val dialog = builder.create()
        dialog.show()
    }

    private fun aboutApp() {
        val builder = AlertDialog.Builder(this@MainActivity)

        val dataVersion = AppSharedPrefs.getInstance(applicationContext).dataVersion
        val message =
            "<h3>" + getString(R.string.app_name) + "</h3> isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends.<p>League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.</p>" +
                    "<p>Designed and Developed by Hisham. :)" +
                    "<p>LoL patch version. <b>" + dataVersion + "</b></p>" +
                    "<p>App version. " + BuildConfig.VERSION_NAME + "</p>" +
                    ""
        builder.setTitle("About the App").setMessage(Html.fromHtml(message))
        val dialog = builder.create()
        dialog.show()
    }

    private fun goToMarketPlace() {
        val uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
                )
            )
        }
    }

    private var mBackPressed: Long = 0
    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else {
                Toast.makeText(baseContext, "Click  back again to exit.", Toast.LENGTH_SHORT).show()
            }
            mBackPressed = System.currentTimeMillis()
        }
    }

    private var wallFragRunning = false
    override fun WallFragRunning() {
        wallFragRunning = true
    }

    override fun WallFragClosed() {
        wallFragRunning = false
    }

    companion object {
        @JvmField
        var championList: ChampionList? = null
        private const val TIME_INTERVAL =
            3000 // # milliseconds, desired time passed between two back presses.
    }

    override fun onFragmentInteraction(uri: Uri?) {

    }
}