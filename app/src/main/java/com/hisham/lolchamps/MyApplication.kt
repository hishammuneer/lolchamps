package com.hisham.lolchamps

import androidx.multidex.MultiDexApplication
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.helper.FontOverride

/**
 * Created by Hisham on 15/Mar/2017 - 03:05
 */
class MyApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        FontOverride.setDefaultFont(this, "DEFAULT", "AvenirRoman.otf")
        FontOverride.setDefaultFont(this, "SANS_SERIF", "AvenirRoman.otf")

        // load remote configs, clearing shared preferences as new version or data maybe coming
        FirebaseRemoteConfig.getInstance().fetch(60 * 60.toLong()).addOnCompleteListener {
            FirebaseRemoteConfig.getInstance().activate().addOnCompleteListener {
                val instance = AppSharedPrefs.getInstance(applicationContext)
                val version = FirebaseRemoteConfig.getInstance().getLong("version")
                val wallpapersVersion =
                    FirebaseRemoteConfig.getInstance().getLong("wallpapers_version")
                val appSideVersion = instance.appSideVersion
                val appSideWallVersion = instance.wallPapersVersion
                // Values here were creating a bug where they were clearing data because version differs and this was happening after loading
                // the latest data. https://console.firebase.google.com/u/0/project/lolchamps-11338/config
                // this should be used hard refresh
                if (version > appSideVersion) {
                    instance.clearAllSharedPreferences()
                }
                if (wallpapersVersion > appSideWallVersion) {
                    instance.clearWallSharedPreferences()
                }
                instance.saveLongPreference(AppSharedPrefs.APP_SIDE_UPDATE_VERSION, version)
                instance.saveLongPreference(AppSharedPrefs.APP_SIDE_WALL_VERSION, wallpapersVersion)
            }
        }
    }
}