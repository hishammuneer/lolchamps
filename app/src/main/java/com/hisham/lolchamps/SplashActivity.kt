package com.hisham.lolchamps

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.hisham.lolchamps.databinding.ActivitySplashBinding
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.viewmodels.SplashViewModel

class SplashActivity : AppCompatActivity() {

    // view binding
    private lateinit var binding: ActivitySplashBinding

    // view Model
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        // no action bar
        window.requestFeature(Window.FEATURE_NO_TITLE)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[SplashViewModel::class.java]
        val prefs = AppSharedPrefs.getInstance(this@SplashActivity)

        anim3()

        // status text
        viewModel.statusTextLiveData.observe(this) {
            binding.tvIntro.text = getString(it)
        }

        viewModel.statusLiveData.observe(this) {
            if (it) {
                // everything is loaded, lets go to next screen
                moveToNextActivityAfterDeciding()
            } else {
                binding.tvIntro.text = getString(R.string.no_internet_retry_2)
                binding.tvIntro.setOnClickListener {
                    binding.tvIntro.text = getString(R.string.loading)
                    checkInternetAndLoad(prefs)
                }
            }
        }
        checkInternetAndLoad(prefs)
    }

    private fun checkInternetAndLoad(prefs: AppSharedPrefs) {
        if (!Globals.isOnline(this)) {
            binding.tvIntro.text = getString(R.string.no_internet_retry)
            binding.tvIntro.setOnClickListener {
                binding.tvIntro.text = getString(R.string.loading)
                checkInternetAndLoad(prefs)
            }
        } else {
            startLoading(prefs)
        }
    }

    private fun startLoading(prefs: AppSharedPrefs) {
        // start loading
        viewModel.getLatestVersion(prefs)
    }

    private fun moveToNextActivityAfterDeciding() {
        finish()
        startActivity(Intent(applicationContext, MainActivity::class.java))
    }

    @SuppressLint("ObjectAnimatorBinding")
    private fun anim3() {
        val alphaAnimation = ObjectAnimator.ofFloat(
            binding.tvIntro,
            "alpha",
            0.0f,
            1.0f
        )
        alphaAnimation.startDelay = 600
        alphaAnimation.duration = 300
        alphaAnimation.start()
    }
}