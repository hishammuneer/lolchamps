package com.hisham.lolchamps.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.fragments.*
import net.rithms.riot.api.endpoints.static_data.dto.Champion

class ChampDetailsAdapter(
    fm: FragmentManager?,
    private val pager: JazzyViewPager,
    private val champion: Champion
) :
    FragmentStatePagerAdapter(
        fm!!
    ) {
    private val tabs = arrayOf("Intro", "Tips", "Skills", "Skins", "Splash")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val obj = super.instantiateItem(container, position)
        pager.setObjectForPosition(obj, position)
        pager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut)
        return obj
    }

    override fun getPageTitle(position: Int): CharSequence? {
//		return championList.getData().get(tabs[position]).getName();
        return tabs[position]
    }

    override fun getCount(): Int {
//		return championList.getData().size();
        return tabs.size
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return LoreFragment.newInstance(champion)
            1 -> return TipsFragment.newInstance(champion)
            2 -> return SkillsFragment.newInstance(champion)
            3 -> return LoadingSkinsFragment.newInstance(champion)
            4 -> return SkinsFragment.newInstance(champion)
        }
        return LoreFragment.newInstance(champion)
    } //	@Override
}