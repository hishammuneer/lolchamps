package com.hisham.lolchamps.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.fragments.ChampOverViewFragment
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

class ChampOverViewAdapter(
    fm: FragmentManager?,
    private val pager: JazzyViewPager,
    private val championList: ChampionList
) : FragmentStatePagerAdapter(
    fm!!
) {
    private val mKeys: Array<String> = championList.data.keys.toTypedArray()
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val obj = super.instantiateItem(container, position)
        pager.setObjectForPosition(obj, position)
        pager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut)
        return obj
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return championList.data[mKeys[position]]?.name
    }

    override fun getCount(): Int {
        return championList.data.size
    }

    override fun getItem(position: Int): Fragment {
        return ChampOverViewFragment.newInstance(position, championList)
    } //	@Override

}