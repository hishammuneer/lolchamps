package com.hisham.lolchamps.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.fragments.RegionsChampsFragment
import com.hisham.lolchamps.fragments.RegionsExtrasFragment
import com.hisham.lolchamps.helper.RegionStory

class RegionDetailsAdapter     //	public int getItemPosition(Object object) {
//		// Auto-generated method stub
//		return POSITION_NONE;
//	}
    (
    fm: FragmentManager?,
    private val pager: JazzyViewPager,
    private val regionStory: RegionStory
) : FragmentStatePagerAdapter(
    fm!!
) {
    private val tabs = arrayOf("Champions", "Visions")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val obj = super.instantiateItem(container, position)
        pager.setObjectForPosition(obj, position)
        pager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut)
        return obj
    }

    override fun getPageTitle(position: Int): CharSequence? {
//		return championList.getData().get(tabs[position]).getName();
        return tabs[position]
    }

    override fun getCount(): Int {
        return if (regionStory.extras == null || regionStory.extras!!.isEmpty()) {
            tabs.size - 1
        } else tabs.size
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return RegionsChampsFragment.newInstance(regionStory)
            1 -> return RegionsExtrasFragment.newInstance(regionStory)
        }
        return RegionsChampsFragment.newInstance(regionStory)
    } //	@Override
}