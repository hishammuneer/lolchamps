package com.hisham.lolchamps.adapters

import android.app.Activity
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.gson.Gson
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.fragments.RegionFragment
import com.hisham.lolchamps.helper.RegionStoryHelper

class RegionsAdapter(fm: FragmentManager?, private val pager: JazzyViewPager, activity: Activity?) :
    FragmentStatePagerAdapter(
        fm!!
    ) {
    private val regionStoryHelper: RegionStoryHelper? = Gson().fromJson(
        activity?.let { Globals.loadRegionsJSONFromAsset(it) },
        RegionStoryHelper::class.java
    )

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val obj = super.instantiateItem(container, position)
        pager.setObjectForPosition(obj, position)
        pager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut)
        return obj
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (regionStoryHelper?.regions != null) {
            regionStoryHelper.regions!![position].name
        } else "Unable to load regions"
    }

    override fun getCount(): Int {
        return if (regionStoryHelper?.regions != null) {
            regionStoryHelper.regions!!.size
        } else 1
    }

    override fun getItem(position: Int): Fragment {
        return RegionFragment.newInstance(position, regionStoryHelper) // todo change this
    } //	@Override

}