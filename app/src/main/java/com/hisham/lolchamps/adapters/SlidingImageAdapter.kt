package com.hisham.lolchamps.adapters

import android.app.Activity
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import coil.load
import coil.transform.BlurTransformation
import coil.transform.CircleCropTransformation
import com.github.chrisbanes.photoview.PhotoView
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.R
import com.hisham.lolchamps.model.WallItemModel

/**
 * Created by Hisham on 16/Mar/2017 - 04:03
 */
class SlidingImageAdapter(
    private val activity: Activity,
    private val wallItemModels: List<WallItemModel?>,
    private val currentItem: Int,
    private val listener: SlidingAdapterListener
) : PagerAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    interface SlidingAdapterListener {
        fun onSetWallClicked(url: String?)
        fun onDownloadHDClicked(url: String?)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return wallItemModels.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_image_photo_view, view, false)
        val mPhotoView: PhotoView = imageLayout.findViewById<View>(R.id.iv_photo) as PhotoView
        val btnSetWall: Button = imageLayout.findViewById<View>(R.id.btnSetWall) as Button
        val btnDownloadHD: Button = imageLayout.findViewById<View>(R.id.btnDownloadHD) as Button
        mPhotoView.maximumScale = 16f
        mPhotoView.minimumScale = 1f
        val llDescription: ViewGroup = imageLayout.findViewById<View>(R.id.llDescription) as ViewGroup
        val tvDescription: TextView = imageLayout.findViewById<View>(R.id.tvDescription) as TextView
        val tvSize: TextView = imageLayout.findViewById<View>(R.id.tvSize) as TextView
        val pbImage: ProgressBar = imageLayout.findViewById<View>(R.id.pbImage) as ProgressBar
        mPhotoView.setOnClickListener {
            if (tvDescription.isShown) {
                tvDescription.visibility = View.GONE
            } else {
                tvDescription.visibility = View.VISIBLE
            }
            if (btnSetWall.isShown) {
                btnSetWall.visibility = View.GONE
            } else {
                btnSetWall.visibility = View.VISIBLE
            }
            if (btnDownloadHD.isShown) {
                btnDownloadHD.visibility = View.GONE
            } else {
                btnDownloadHD.visibility = View.VISIBLE
            }
        }
        btnSetWall.setOnClickListener { listener.onSetWallClicked(wallItemModels[position]?.link) }
        btnDownloadHD.setOnClickListener { listener.onDownloadHDClicked(wallItemModels[position]?.link) }
        //        mCurrMatrixTv = (TextView) findViewById(R.id.tv_current_matrix);

//        Drawable bitmap = ContextCompat.getDrawable(this, R.drawable.wallpaper);
//        mPhotoView.setImageDrawable(bitmap);
        if (!TextUtils.isEmpty(wallItemModels[position]?.title)) {
//                llDescription.setVisibility(View.VISIBLE);
            tvDescription.text = wallItemModels[position]?.title ?: ""
        } else {
//                llDescription.setVisibility(View.GONE);
            tvDescription.text = ""
        }
        val size = wallItemModels[position]?.size
        if (size != null) {
            val fileSize = "Size: " + Globals.getFileSize(size.toLong())
            tvSize.text = fileSize
        }
        var imageLink = wallItemModels[position]?.thumbLink
        if (imageLink == null) {
            imageLink = wallItemModels[position]?.link
        }

//        Globals.loadImage(activity, imageLink!!, null, mPhotoView)
        mPhotoView.load(imageLink) {
            crossfade(true)
            placeholder(R.drawable.place_holder)
        }
        //        mPhotoView.setOnPhotoTapListener(new ImageActivity.PhotoTapListener());
//        mPhotoView.setOnSingleFlingListener(new ImageActivity.SingleFlingListener());
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }

}