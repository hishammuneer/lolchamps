package com.hisham.lolchamps.adapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hisham.jazzyviewpagerlib.JazzyViewPager
import com.hisham.lolchamps.fragments.WallRecentsFragment
import com.hisham.lolchamps.utils.AppConstants

class WallAdapter     //	public int getItemPosition(Object object) {
//		// Auto-generated method stub
//		return POSITION_NONE;
//	}
    (fm: FragmentManager?, private val pager: JazzyViewPager) : FragmentStatePagerAdapter(
    fm!!
) {
    //    private String[] tabs = new String[]{"Recent", "Poros", "Fan Art"};
    private val tabs = arrayOf("Recent")

    //	@Override
    var wallFrag: WallRecentsFragment? = null
        private set

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val obj = super.instantiateItem(container, position)
        pager.setObjectForPosition(obj, position)
        pager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut)
        return obj
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabs[position]
    }

    override fun getCount(): Int {
        return tabs.size
    }

    override fun getItem(position: Int): Fragment {
        wallFrag = WallRecentsFragment.newInstance(AppConstants.RECENTS, tabs[0])
        return wallFrag ?: Fragment()
    }
}