package com.hisham.lolchamps.compose.colors

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val DarkLight = Color(0xff2a2a2a)
val DarkBlack = Color(0xff212121)
val Gold = Color(0xff937341)
val GoldShine = Color(0xFFB08A4F)
val GoldTransparent = Color(0xCCB08A4F)

val Whitish = Color(0xFFE3E3E3)
val PureWhite = Color(0xFFFFFFFF)
val GoldDark = Color(0xFF765828)


val lolLightColors = lightColors(
    primary = DarkLight,
    primaryVariant = DarkBlack,
    secondary = Gold,
    secondaryVariant = GoldShine,
    background = DarkLight
)
val lolDarkColors = darkColors(
    primary = Whitish,
    primaryVariant = PureWhite,
    secondary = GoldDark,
    secondaryVariant = GoldDark,
    background = Whitish
)
