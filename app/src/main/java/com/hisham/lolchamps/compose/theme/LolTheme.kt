package com.hisham.lolchamps.compose.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import com.hisham.lolchamps.compose.colors.lolDarkColors
import com.hisham.lolchamps.compose.colors.lolLightColors

@Composable
fun LolTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = if (darkTheme) lolLightColors else lolDarkColors,
        content = content
    )
}