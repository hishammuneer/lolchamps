/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hisham.lolchamps.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.ChampDetailActivity
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.R
import com.hisham.lolchamps.interfaces.IChampion
import com.hisham.lolchamps.server.ServerHit
import com.hisham.lolchamps.utils.CompatibilityUtil
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.Champion
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

class ChampOverViewFragment : Fragment() {
    private var position = 0
    private var champion: Champion? = null
    private var keys: Array<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = requireArguments().getInt(ARG_POSITION)
        val championList = requireArguments().getSerializable(CHAMP_LIST) as ChampionList?
        if (championList != null) {
            keys = championList.data.keys.toTypedArray()
            champion = championList.data[keys!![position]]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // #77797d
        val view = inflater.inflate(R.layout.champ_overview_frag_layout, container, false)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        val imgBack = view.findViewById<View>(R.id.imgBack) as ImageView
        val llTags = view.findViewById<View>(R.id.llTags) as LinearLayout
        //        reArrange(imgBack);
        imgBack.setOnClickListener { loadChampion() }
        val imgUrl = ConstUrl.BASE_URL + "cdn/img/champion/splash/" + champion!!.id + "_0.jpg"
        Globals.loadImage(imgUrl, imgBack)
        val tvcard = view.findViewById<View>(R.id.tvCardNo) as TextView
        // tvcard.setText("Card" + (position + 1));
        var content = ""
        // if Main title is present
        if (position < keys?.size ?: 0) {
            content += "<h3>" + champion!!.name + "</h3>"
            content += "<h6>" + champion!!.title + "</h6>"
        }

        // if content is present
        if (position < keys?.size ?: 0) {
            content += "<p>" + champion!!.blurb + "</p>"
        }
        tvcard.text = Html.fromHtml(content)
        if (champion!!.tags != null && champion!!.tags.size > 0) {
            llTags.removeAllViews()
            for (tag in champion!!.tags) {
                val tagView = inflater.inflate(R.layout.tag_layout, container, false)
                val imgTag = tagView.findViewById<View>(R.id.imgTag) as ImageView
                val tvTag = tagView.findViewById<View>(R.id.tvTag) as TextView
                Globals.setTagImage(tag, imgTag)
                tvTag.text = tag
                llTags.addView(tagView)
            }
        }
        val tvMore = view.findViewById<View>(R.id.tvMore) as TextView
        tvMore.setOnLongClickListener {
            Toast.makeText(
                activity,
                "Load more details about " + champion!!.name,
                Toast.LENGTH_SHORT
            ).show()
            true
        }
        tvMore.setOnClickListener { loadChampion() }

//		final RiotApi api = new RiotApi();
//		api.setRegion(Region.EUW);
//
//		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
//
//		if(prefs.contains("champData")){
//			championList = new Gson().fromJson(prefs.getString("champData", null), net.rithms.riot.dto.Static.ChampionList.class);
//		} else {
//			//Body of your click handler
//			Thread thread = new Thread(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						championList = api.getDataChampionList(null, null, false, ChampData.ALL);
//						String champList = new Gson().toJson(championList);
//						prefs.edit().putString("champData", champList).apply();
//						if (championList != null) {
//							Log.d(TAG, "onCreateView: " + championList);
//						}
//					} catch (RiotApiException e) {
//						e.printStackTrace();
//					}
//				}
//			});
//			thread.start();
//		}
        return view
    }

    private fun loadChampion() {
        val progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Loading. Please wait...")
        progressDialog.isIndeterminate = false
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
        progressDialog.show()
        ServerHit.instance.getChampionByKey(requireActivity(), champion!!.id, object : IChampion {
            override fun onChampionLoaded(champion: Champion?) {
                progressDialog.dismiss()
                if (champion == null) {
                    Toast.makeText(
                        activity,
                        "Unable to load data, please try again later.",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                val intent = Intent(activity, ChampDetailActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("champion", champion)
                startActivity(intent.putExtras(bundle))
            }
        })
    }

    private fun reArrange(imagebg: ImageView) {
        if (CompatibilityUtil.isTablet(requireContext())) {
            imagebg.layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
            imagebg.layoutParams.height = 600
            return
        }
        if (CompatibilityUtil.isInLandScapce(requireContext())) {
            imagebg.layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
            imagebg.layoutParams.height = 400
        }
    }

    companion object {
        private val TAG = ChampOverViewFragment::class.java.simpleName
        private const val CHAMP_LIST = "champ_list"
        private const val ARG_POSITION = "position"
        fun newInstance(position: Int, championList: ChampionList?): ChampOverViewFragment {
            val f = ChampOverViewFragment()
            val b = Bundle()
            b.putInt(ARG_POSITION, position)
            b.putSerializable(CHAMP_LIST, championList)
            f.arguments = b
            return f
        }
    }
}