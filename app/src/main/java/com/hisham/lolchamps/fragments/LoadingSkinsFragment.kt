package com.hisham.lolchamps.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hisham.lolchamps.*
import com.hisham.lolchamps.helper.*
import com.hisham.lolchamps.helper.DownloadHelper.download
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.Champion
import net.rithms.riot.api.endpoints.static_data.dto.Skin

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoadingSkinsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoadingSkinsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoadingSkinsFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var champion: Champion? = null
    private var recyclerView: RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            champion = requireArguments().getSerializable(CHAMP) as Champion?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_containing_recycler_view, container, false)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        recyclerView = view.findViewById(R.id.card_recycler_view)
        recyclerView?.setHasFixedSize(true)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(activity, 2)
        recyclerView?.setLayoutManager(layoutManager)
        val adapter: DataAdapter = DataAdapter(
            activity?.applicationContext!!, champion!!.skins
        )
        recyclerView?.adapter = adapter
        return view
    }

    inner class DataAdapter(private val context: Context, private val skins: List<Skin>) :
        RecyclerView.Adapter<DataAdapter.ViewHolder>() {
        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.row_splash_skins, viewGroup, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
            var skinName = skins[i].name
            if (skinName.equals("default", ignoreCase = true)) {
                skinName = champion!!.name
            }
            viewHolder.tvSkinName.text = skinName
            val imgUrl =
                ConstUrl.BASE_URL + "cdn/img/champion/loading/" + champion!!.id + "_" + champion!!.skins[i].num + ".jpg"
            Globals.loadImage(imgUrl, viewHolder.imgBack)
            val skinUrl =
                ConstUrl.BASE_URL + "cdn/img/champion/loading/" + champion!!.id + "_" + skins[i].num + ".jpg"
            viewHolder.imgBack.setOnClickListener {
                val intent = Intent(activity, ImageActivity::class.java)
                val bundle = Bundle()
                bundle.putString("imageLink", skinUrl)
                var skinName = champion!!.skins[i].name
                if (skinName.equals("default", ignoreCase = true)) {
                    skinName = champion!!.name
                }
                bundle.putString("title", "Skins")
                bundle.putString("description", skinName)
                startActivity(intent.putExtras(bundle))
            }
            viewHolder.imgBack.setOnLongClickListener {
                if (activity != null) {
                    PermissionHelper().requestPermission(
                        activity!!,
                        object : PermissionHelperListener {
                            override fun onPermission(granted: Boolean) {
                                if (granted) {
                                    download(activity!!, skinUrl, null)
                                    Toast.makeText(
                                        context,
                                        R.string.downloading_check_notifications,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        },
                        recyclerView
                    )
                }
                true
            }
        }

        override fun getItemCount(): Int {
            return skins.size
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tvSkinName: TextView = view.findViewById<View>(R.id.tvSkinName) as TextView
            val imgBack: ImageView = view.findViewById<View>(R.id.imgBack) as ImageView
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val CHAMP = "champion"
        fun newInstance(champ: Champion?): LoadingSkinsFragment {
            val fragment = LoadingSkinsFragment()
            val args = Bundle()
            args.putSerializable(CHAMP, champ)
            fragment.arguments = args
            return fragment
        }
    }
}