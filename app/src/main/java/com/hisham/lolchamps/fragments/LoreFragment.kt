package com.hisham.lolchamps.fragments

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.hisham.lolchamps.R
import net.rithms.riot.api.endpoints.static_data.dto.Champion
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoreFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoreFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var champion: Champion? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            champion = requireArguments().getSerializable(CHAMP) as Champion?
        }
    }

    private var mChart: PieChart? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_lore, container, false)
        statsSetup(view)
        pieChartSetup(view)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        val tvLore = view.findViewById<View>(R.id.tvLore) as TextView
        var content = "<h3>" + champion!!.name + "</h3>"
        content += "<h6>" + champion!!.title + "</h6>"
        content += "<p>" + champion!!.lore + "</p>"
        tvLore.text = Html.fromHtml(content)
        return view
    }

    private fun statsSetup(view: View) {
        val tvStats = view.findViewById<View>(R.id.tvStats) as TextView
        var data = "<h2>Stats</h2><p>"
        val stats = champion!!.stats
        data += "Health: " + stats.hp + " (" + stats.hpPerLevel + " per level)<br/>"
        if (stats.mpPerLevel != 0.0) {
            data += "Mana: " + stats.mp + " (" + stats.mpPerLevel + " per level)<br/>"
        }
        data += "Attack: " + stats.attackDamage + " (" + stats.attackDamagePerLevel + " per level)<br/>"
        data += "Armor: " + stats.armor + " (" + stats.armorPerLevel + " per level)<br/>"
        data += "Range: " + stats.attackRange + "<br/><br/>"
        data += "Health Regeneration: " + stats.hpRegen + " (" + stats.hpRegenPerLevel + " per level)<br/>"
        if (stats.mpRegenPerLevel != 0.0) {
            data += "Mana Regeneration: " + stats.mpregen + " (" + stats.mpRegenPerLevel + " per level)<br/>"
        }
        data += "Attack Speed: " + stats.baseAttackSpeed + " (+" + stats.attackSpeedPerLevel + "% per level)<br/>"
        data += "Magic Resist: " + stats.spellBlock + " (" + stats.spellBlockPerLevel + " per level)<br/>"
        data += "Movement: " + stats.moveSpeed
        data += "</p>"
        tvStats.text = Html.fromHtml(data)
    }

    private fun pieChartSetup(view: View) {
        mChart = view.findViewById<View>(R.id.pieChart) as PieChart

        ///////////////////////////////////////////////////////////////////////////
        // pie chart
        mChart!!.setUsePercentValues(false)
        mChart!!.description.isEnabled = false
        mChart!!.setExtraOffsets(5f, 10f, 5f, 5f)
        mChart!!.dragDecelerationFrictionCoef = 0.95f

//        mChart.setCenterTextTypeface(mTfLight);
//        mChart.setCenterText(generateCenterSpannableText());
        mChart!!.isDrawHoleEnabled = true
        mChart!!.setHoleColor(requireActivity().resources.getColor(R.color.page_background))
        mChart!!.setTransparentCircleColor(requireActivity().resources.getColor(R.color.page_background))
        mChart!!.setTransparentCircleAlpha(110)
        mChart!!.holeRadius = 20f
        mChart!!.transparentCircleRadius = 24f
        mChart!!.setDrawCenterText(true)
        mChart!!.centerText = "Stats"
        mChart!!.setCenterTextColor(Color.WHITE)
        mChart!!.rotationAngle = 0f
        // enable rotation of the chart by touch
//        mChart.setRotationEnabled(true);
        mChart!!.isHighlightPerTapEnabled = true

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
//        mChart.setOnChartValueSelectedListener(this);
        setData()
        mChart!!.animateY(1400, Easing.EasingOption.EaseInOutQuad)
        // mChart.spin(2000, 0, 360);
        val l = mChart!!.legend
        l.verticalAlignment =
            Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment =
            Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.xEntrySpace = 8f
        l.yEntrySpace = 8f
        l.yOffset = 8f
        l.xOffset = 8f
        l.textColor = Color.WHITE
        l.textSize = 12f

        // entry label styling
//        mChart.setDrawEntryLabels(false);
        mChart!!.setEntryLabelColor(Color.WHITE)
        mChart!!.setEntryLabelTextSize(12f)


        ///////////////////////////////////////////////////////////////////////////
    }

    private fun setData() {
        val entries = ArrayList<PieEntry>()
        entries.add(PieEntry(champion!!.info.attack.toFloat(), "Attack: " + champion!!.info.attack))
        entries.add(PieEntry(champion!!.info.defense.toFloat(), "Defense: " + champion!!.info.defense))
        entries.add(PieEntry(champion!!.info.magic.toFloat(), "Magic: " + champion!!.info.magic))
        entries.add(
            PieEntry(
                champion!!.info.difficulty.toFloat(),
                "Difficulty: " + champion!!.info.difficulty
            )
        )
        val dataSet = PieDataSet(entries, "")
        dataSet.setDrawIcons(true)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0F, 40F)
        dataSet.selectionShift = 5f

        // add a lot of colors
        val colors = ArrayList<Int>()
        for (c in ColorTemplate.MATERIAL_COLORS) colors.add(c)
        dataSet.colors = colors
        val data = PieData(dataSet)
        //        data.setValueFormatter(new DefaultValueFormatter(0));
//        data.setValueTextSize(16f);
//        data.setValueTextColor(Color.WHITE);
        data.setDrawValues(false)
        mChart!!.data = data
        // undo all highlights
        mChart!!.highlightValues(null)
        mChart!!.invalidate()
    }

    fun onButtonPressed(uri: Uri?) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val CHAMP = "champ"
        fun newInstance(champ: Champion?): LoreFragment {
            val fragment = LoreFragment()
            val args = Bundle()
            args.putSerializable(CHAMP, champ)
            fragment.arguments = args
            return fragment
        }
    }
}