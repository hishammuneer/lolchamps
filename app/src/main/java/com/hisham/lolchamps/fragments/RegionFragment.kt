/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hisham.lolchamps.fragments

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.*
import com.hisham.lolchamps.helper.*
import com.hisham.lolchamps.helper.DownloadHelper.download

class RegionFragment : Fragment() {
    private var position = 0
    private var regionStory: RegionStory? = null
    private var scrollView: ScrollView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = requireArguments().getInt(ARG_POSITION)
        val regionStoryHelper =
            requireArguments().getSerializable(REGION_STORY_HELPER) as RegionStoryHelper?
        if (regionStoryHelper?.regions != null) {
            regionStory = regionStoryHelper.regions!![position]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.region_frag_layout, container, false)
        if (regionStory == null) {
            Toast.makeText(activity, "Unable to load regions", Toast.LENGTH_SHORT).show()
            return view
        }
        scrollView = view.findViewById(R.id.scrollView1)
        val tvRegionName = view.findViewById<View>(R.id.tvRegionName) as TextView
        val tvShowMore = view.findViewById<View>(R.id.tvShowMore) as TextView
        val imgTag = view.findViewById<View>(R.id.imgTag) as ImageView
        regionStory!!.crestIcon?.let {
            Globals.loadImageWithInvisibility(
                it,
                imgTag
            )
        }
        val face = Typeface.createFromAsset(requireActivity().assets, "Beaufort-Bold.ttf")
        tvRegionName.setTypeface(face)
        tvRegionName.isAllCaps = true
        tvRegionName.text = regionStory!!.name
        val imgBack = view.findViewById<View>(R.id.imgBack) as ImageView
        imgBack.setOnClickListener { showRegion() }
        tvShowMore.setOnClickListener { showRegion() }
        imgBack.setOnLongClickListener {
            val intent = Intent(activity, ImageActivity::class.java)
            val bundle = Bundle()
            bundle.putString("imageLink", regionStory!!.coverImageUrl)
            bundle.putString("title", "Regions")
            bundle.putString("description", regionStory!!.name)
            startActivity(intent.putExtras(bundle))
            true
        }
        imgBack.setOnLongClickListener {
            if (activity != null) {
                PermissionHelper().requestPermission(
                    requireActivity(),
                    object : PermissionHelperListener {
                        override fun onPermission(granted: Boolean) {
                            if (granted) {
                                regionStory!!.coverImageUrl?.let { it1 ->
                                    download(activity!!,
                                        it1, null)
                                }
                                Toast.makeText(
                                    activity,
                                    R.string.downloading_check_notifications,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    },
                    scrollView
                )
            }
            true
        }
        regionStory!!.coverImageUrl?.let {
            Globals.loadImageWithInvisibility(
                it,
                imgBack
            )
        }
        val tvcard = view.findViewById<View>(R.id.tvCardNo) as TextView
        // tvcard.setText("Card" + (position + 1));
        tvcard.text = Html.fromHtml(regionStory!!.story)
        return view
    }

    private fun showRegion() {
        val intent = Intent(activity, RegionDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("region", regionStory)
        startActivity(intent.putExtras(bundle))
    }

    companion object {
        private val TAG = RegionFragment::class.java.simpleName
        private const val REGION_STORY_HELPER = "REGION_STORY_HELPER"
        private const val ARG_POSITION = "position"
        fun newInstance(position: Int, regionStoryHelper: RegionStoryHelper?): RegionFragment {
            val f = RegionFragment()
            val b = Bundle()
            b.putInt(ARG_POSITION, position)
            b.putSerializable(REGION_STORY_HELPER, regionStoryHelper)
            f.arguments = b
            return f
        }
    }
}