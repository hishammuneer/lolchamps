package com.hisham.lolchamps.fragments

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.*
import com.hisham.lolchamps.helper.RegionStory
import com.hisham.lolchamps.helper.StoryChampions
import com.hisham.lolchamps.interfaces.IChampion
import com.hisham.lolchamps.server.ServerHit
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.Champion

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RegionsChampsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RegionsChampsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegionsChampsFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var regionStory: RegionStory? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            regionStory = requireArguments().getSerializable(REGION) as RegionStory?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_skins, container, false)
        if (regionStory == null) {
            Toast.makeText(activity, "Unable to load region.", Toast.LENGTH_SHORT).show()
            return view
        }
        val listSkins = view.findViewById<View>(R.id.listSkins) as ListView

        // showing champions
            val adapter = ChampionAdapter(requireActivity(), R.layout.row_champs_region,
                regionStory!!.champions!!
            )
            listSkins.adapter = adapter
        return view
    }

    private inner class ChampionAdapter(
        context: Context,
        @param:LayoutRes private val resource: Int,
        private val storyChampionsList: List<StoryChampions>
    ) : ArrayAdapter<StoryChampions?>(context, resource, storyChampionsList) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var rowView = convertView
            val viewHolder: ViewHolder
            if (rowView == null) {
                val inflater =
                    activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                rowView = inflater.inflate(resource, null)
                // configure view holder
                viewHolder = ViewHolder()
                viewHolder.tvSkinName = rowView.findViewById<View>(R.id.tvSkinName) as TextView
                viewHolder.imgBack = rowView.findViewById<View>(R.id.imgBack) as ImageView
                rowView.tag = viewHolder
            } else {
                viewHolder = rowView.tag as ViewHolder
            }
            var champion: Champion? = null
            val mKeys = MainActivity.championList?.data?.keys?.toTypedArray()
            if (mKeys != null) {
                for (mKey in mKeys) {
                    val champ = MainActivity.championList?.data?.get(mKey)
                    if (champ?.name.equals(
                            storyChampionsList[position].name, ignoreCase = true
                        )
                    ) {
                        champion = champ
                        break
                    }
                }
            }
            if (champion != null) {
                val champLoadingUrl =
                    ConstUrl.BASE_URL + "cdn/img/champion/splash/" + champion.id + "_0.jpg"
                viewHolder.imgBack?.let {
                    Globals.loadImage(
                        champLoadingUrl, it
                    )
                }
                viewHolder.tvSkinName!!.text = storyChampionsList[position].name
                val finalChampion: Champion = champion
                rowView!!.setOnClickListener { loadChampion(finalChampion) }
            }
            return rowView!!
        }

        inner class ViewHolder {
            var tvSkinName: TextView? = null
            var imgBack: ImageView? = null
            private val pbImage: ProgressBar? = null
        }

        private fun loadChampion(champion: Champion) {
            val progressDialog = ProgressDialog(activity)
            progressDialog.setMessage("Loading. Please wait...")
            progressDialog.isIndeterminate = false
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog.setCancelable(false)
            progressDialog.show()
            activity?.let {
                ServerHit.instance.getChampionByKey(it, champion.id, object : IChampion {
                    override fun onChampionLoaded(champion: Champion?) {
                        progressDialog.dismiss()
                        if (champion == null) {
                            Toast.makeText(
                                activity,
                                "Unable to load data, please try again later.",
                                Toast.LENGTH_SHORT
                            ).show()
                            return
                        }
                        val intent = Intent(activity, ChampDetailActivity::class.java)
                        val bundle = Bundle()
                        bundle.putSerializable("champion", champion)
                        startActivity(intent.putExtras(bundle))
                    }
                })
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val REGION = "champion"
        fun newInstance(regionStory: RegionStory?): RegionsChampsFragment {
            val fragment = RegionsChampsFragment()
            val args = Bundle()
            args.putSerializable(REGION, regionStory)
            fragment.arguments = args
            return fragment
        }
    }
}