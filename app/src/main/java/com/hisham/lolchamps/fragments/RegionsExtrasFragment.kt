package com.hisham.lolchamps.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.*
import com.hisham.lolchamps.helper.*
import com.hisham.lolchamps.helper.DownloadHelper.download

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RegionsExtrasFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RegionsExtrasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegionsExtrasFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var regionStory: RegionStory? = null
    private var listSkins: ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            regionStory = requireArguments().getSerializable(REGION) as RegionStory?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_skins, container, false)
        if (regionStory == null) {
            Toast.makeText(activity, "Unable to load region.", Toast.LENGTH_SHORT).show()
            return view
        }
        listSkins = view.findViewById(R.id.listSkins)

        // showing extras
            val adapter =
                regionStory!!.extras?.let {
                    RegionExtrasAdapter(requireActivity(), R.layout.row_extras,
                        it
                    )
                }
        listSkins?.adapter = adapter
        return view
    }

    private inner class RegionExtrasAdapter(
        context: Context,
        @param:LayoutRes private val resource: Int,
        private val storyExtrases: List<StoryExtras>
    ) : ArrayAdapter<StoryExtras?>(context, resource, storyExtrases) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var rowView = convertView
            val viewHolder: ViewHolder
            if (rowView == null) {
                val inflater =
                    activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                rowView = inflater.inflate(resource, null)
                // configure view holder
                viewHolder = ViewHolder()
                viewHolder.tvSkinName = rowView.findViewById<View>(R.id.tvSkinName) as TextView
                viewHolder.imgBack = rowView.findViewById<View>(R.id.imgBack) as ImageView
                rowView.tag = viewHolder
            } else {
                viewHolder = rowView.tag as ViewHolder
            }

            // showing extras
            if (regionStory!!.extras != null && regionStory!!.extras?.size ?: 0 > 0) {
                activity?.let {
                    regionStory!!.extras?.get(position)?.url?.let { it1 ->
                        viewHolder.imgBack?.let { it2 ->
                            Globals.loadImage(
                                it1,
                                it2
                            )
                        }
                    }
                }
                viewHolder.tvSkinName!!.text = regionStory!!.extras?.get(position)?.description ?: ""
                rowView!!.setOnClickListener {
                    val intent = Intent(activity, ImageActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("imageLink", regionStory!!.extras?.get(position)?.url)
                    bundle.putString("title", regionStory!!.name)
                    bundle.putString("description",
                        regionStory!!.extras?.get(position)?.description
                    )
                    startActivity(intent.putExtras(bundle))
                }
                rowView.setOnLongClickListener {
                    if (activity != null) {
                        PermissionHelper().requestPermission(
                            activity!!,
                            object : PermissionHelperListener {
                                override fun onPermission(granted: Boolean) {
                                    if (granted) {
                                        regionStory!!.extras?.get(position)?.url?.let { it1 ->
                                            download(
                                                activity!!,
                                                it1,
                                                null
                                            )
                                        }
                                        Toast.makeText(
                                            activity,
                                            R.string.downloading_check_notifications,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            },
                            listSkins
                        )
                    }
                    true
                }
            } else {
            }
            return rowView!!
        }

        inner class ViewHolder {
            var tvSkinName: TextView? = null
            var imgBack: ImageView? = null
            private val pbImage: ProgressBar? = null
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val REGION = "champion"
        fun newInstance(regionStory: RegionStory?): RegionsExtrasFragment {
            val fragment = RegionsExtrasFragment()
            val args = Bundle()
            args.putSerializable(REGION, regionStory)
            fragment.arguments = args
            return fragment
        }
    }
}