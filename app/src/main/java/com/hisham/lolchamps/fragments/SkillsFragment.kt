package com.hisham.lolchamps.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.R
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.Champion

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SkillsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SkillsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SkillsFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var champion: Champion? = null
    private var inflater1: LayoutInflater? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            champion = requireArguments().getSerializable(CHAMP) as Champion?
        }
        inflater1 = requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_skills, container, false)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        val llSpells = view.findViewById<View>(R.id.llSpells) as ViewGroup
        llSpells.removeAllViews()

        // add passive
        val passiveSpellView = inflater1!!.inflate(R.layout.spells_layout, null)
        val ivPassiveSpell = passiveSpellView.findViewById<View>(R.id.ivSpell) as ImageView
        val tvPassiveSpellDescription =
            passiveSpellView.findViewById<View>(R.id.tvSpellDescription) as TextView
        val tvPassiveSpellTitle = passiveSpellView.findViewById<View>(R.id.tvSpellTitle) as TextView
        val passiveLink =
            ConstUrl.BASE_URL + "cdn/" + activity?.let { AppSharedPrefs.getInstance(it).dataVersion } + "/img/passive/" + champion!!.passive.image.full
        tvPassiveSpellTitle.text = champion!!.passive.name + " (Passive)"
        var passiveContent = ""
        passiveContent += "<p>" + champion!!.passive.description + "</p>"
        tvPassiveSpellDescription.text = Html.fromHtml(passiveContent)
        Globals.loadImage(passiveLink.trim { it <= ' ' }, ivPassiveSpell)
        llSpells.addView(passiveSpellView)


        // spells details
        for (i in champion!!.spells.indices) {
            val spellView = inflater1!!.inflate(R.layout.spells_layout, null)
            val championSpell = champion!!.spells[i]
            // link
            val spellImageLink =
                ConstUrl.BASE_URL + "cdn/" + activity?.let { AppSharedPrefs.getInstance(it).dataVersion } + "/img/spell/" + championSpell.image.full
            val ivSpell = spellView.findViewById<View>(R.id.ivSpell) as ImageView
            val tvSpellDescription =
                spellView.findViewById<View>(R.id.tvSpellDescription) as TextView
            val tvSpellTitle = spellView.findViewById<View>(R.id.tvSpellTitle) as TextView
            val divider = spellView.findViewById<View>(R.id.divider)
            // hiding divider after last item.
            if (i == champion!!.spells.size - 1) {
                divider.visibility = View.GONE
            } else {
                divider.visibility = View.VISIBLE
            }
            val button = Globals.getButtonCharBasedOnPosition(i)
            //            tvSpellTitle.setText(Html.fromHtml("<h3>"+ championSpell.getName() + button + "</h3>"));
            tvSpellTitle.text = championSpell.name + "" + button
            var spellContent = ""
            spellContent += "<p>" + championSpell.description + "</p>"
            tvSpellDescription.text = Html.fromHtml(spellContent)
            Globals.loadImage(spellImageLink.trim { it <= ' ' }, ivSpell)
            llSpells.addView(spellView)
        }
        return view
    }

    fun onButtonPressed(uri: Uri?) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val CHAMP = "champ"
        fun newInstance(champ: Champion?): SkillsFragment {
            val fragment = SkillsFragment()
            val args = Bundle()
            args.putSerializable(CHAMP, champ)
            fragment.arguments = args
            return fragment
        }
    }
}