package com.hisham.lolchamps.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemLongClickListener
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.*
import com.hisham.lolchamps.helper.*
import com.hisham.lolchamps.helper.DownloadHelper.download
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.Champion
import net.rithms.riot.api.endpoints.static_data.dto.Skin

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SkinsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SkinsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SkinsFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var champion: Champion? = null
    private var listSkins: ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            champion = requireArguments().getSerializable(CHAMP) as Champion?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_skins, container, false)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        listSkins = view.findViewById(R.id.listSkins)
        if (activity != null) {
            val adapter = SkinAdapter(requireActivity(), R.layout.row_skins, champion!!.skins)
            listSkins?.setAdapter(adapter)
        }
        listSkins?.setOnItemClickListener(OnItemClickListener { adapterView, view, position, l ->
            val intent = Intent(activity, ImageActivity::class.java)
            val bundle = Bundle()
            val imgUrl =
                ConstUrl.BASE_URL + "cdn/img/champion/splash/" + champion!!.id + "_" + champion!!.skins[position].num + ".jpg"
            bundle.putString("imageLink", imgUrl)
            var skinName = champion!!.skins[position].name
            if (skinName.equals("default", ignoreCase = true)) {
                skinName = champion!!.name
            }
            bundle.putString("title", "Skins")
            bundle.putString("description", skinName)
            startActivity(intent.putExtras(bundle))
        })
        listSkins?.setOnItemLongClickListener(OnItemLongClickListener { parent, view, position, id ->
            val skinUrl =
                ConstUrl.BASE_URL + "cdn/img/champion/splash/" + champion!!.id + "_" + champion!!.skins[position].num + ".jpg"
            if (activity != null) {
                PermissionHelper().requestPermission(
                    requireActivity(),
                    object : PermissionHelperListener {
                        override fun onPermission(granted: Boolean) {
                            if (granted) {
                                download(activity!!, skinUrl, null)
                                Toast.makeText(
                                    activity,
                                    R.string.downloading_check_notifications,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    },
                    listSkins
                )
            }
            true
        })


//        init(view, champion);

//        LinearLayout llTags = (LinearLayout) view.findViewById(R.id.llTags);
//        reArrange(imgBack);

//        TextView tvcard = (TextView) view.findViewById(R.id.tvCardNo);
        // tvcard.setText("Card" + (position + 1));
        var content = ""
        // if Main title is present
        content += "<h3>" + champion!!.name + "</h3>"
        content += "<h6>" + champion!!.title + "</h6>"

        // if content is present
        content += "<p>" + champion!!.lore + "</p>"

//        tvcard.setText(Html.fromHtml(content));

//        if (champion.getTags() != null && champion.getTags().size() > 0) {
//            llTags.removeAllViews();
//            for (String tag : champion.getTags()) {
//
//                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View tagView = layoutInflater.inflate(R.layout.tag_layout, null);
//                ImageView imgTag = (ImageView) tagView.findViewById(R.id.imgTag);
//                TextView tvTag = (TextView) tagView.findViewById(R.id.tvTag);
//
//                Globals.setTagImage(tag, imgTag);
//                tvTag.setText(tag);
//                llTags.addView(tagView);
//            }
//        }
        return view
    }

    private inner class SkinAdapter(
        context: Context,
        @param:LayoutRes private val resource: Int,
        private val skinsList: List<Skin>
    ) : ArrayAdapter<Skin?>(context, resource, skinsList) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var rowView = convertView
            val viewHolder: ViewHolder
            if (rowView == null) {
                val inflater =
                    activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                rowView = inflater.inflate(resource, null)
                // configure view holder
                viewHolder = ViewHolder()
                viewHolder.tvSkinName = rowView.findViewById<View>(R.id.tvSkinName) as TextView
                viewHolder.imgBack = rowView.findViewById<View>(R.id.imgBack) as ImageView
                viewHolder.pbImage = rowView.findViewById<View>(R.id.pbImage) as ProgressBar
                rowView.tag = viewHolder
            } else {
                viewHolder = rowView.tag as ViewHolder
            }
            if (position == 0) {
                viewHolder.tvSkinName!!.text = champion!!.name
            } else {
                viewHolder.tvSkinName!!.text = champion!!.skins[position].name
            }

//            viewHolder.imgBack.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Globals.setAsBG(getActivity(), viewHolder.imgBack);
//                }
//
//            });
            val imgUrl =
                ConstUrl.BASE_URL + "cdn/img/champion/splash/" + champion!!.id + "_" + champion!!.skins[position].num + ".jpg"
            viewHolder.imgBack?.let {
                Globals.loadImage(
                    imgUrl, it
                )
            }
            return rowView!!
        }

        inner class ViewHolder {
            var tvSkinName: TextView? = null
            var imgBack: ImageView? = null
            var pbImage: ProgressBar? = null
        }
    }

    //    private void init(View view, final Champion champion) {
    //        ViewPager mPager = (ViewPager) view.findViewById(R.id.pager);
    //        mPager.setAdapter(new SlidingImageAdapter(getActivity(),champion));
    //        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
    //        indicator.setViewPager(mPager);
    //        final float density = getResources().getDisplayMetrics().density;
    //        //Set circle indicator radius
    //        indicator.setRadius(5 * density);
    //
    //        final TextView tvSkinName = (TextView) view.findViewById(R.id.tvSkinName);
    //        tvSkinName.setText(champion.getName());
    //        // Pager listener over indicator
    //        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
    //
    //            @Override
    //            public void onPageSelected(int position) {
    //                if(position == 0){
    //                    tvSkinName.setText(champion.getName());
    //                } else {
    //                    tvSkinName.setText(champion.getSkins().get(position).getName());
    //                }
    //            }
    //
    //            @Override
    //            public void onPageScrolled(int pos, float arg1, int arg2) {
    //
    //            }
    //
    //            @Override
    //            public void onPageScrollStateChanged(int pos) {
    //
    //            }
    //        });
    //
    //    }
    fun onButtonPressed(uri: Uri?) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val CHAMP = "champion"
        fun newInstance(champ: Champion?): SkinsFragment {
            val fragment = SkinsFragment()
            val args = Bundle()
            args.putSerializable(CHAMP, champ)
            fragment.arguments = args
            return fragment
        }
    }
}