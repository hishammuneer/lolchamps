package com.hisham.lolchamps.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.hisham.lolchamps.R
import net.rithms.riot.api.endpoints.static_data.dto.Champion

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TipsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TipsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TipsFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var champion: Champion? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            champion = requireArguments().getSerializable(CHAMP) as Champion?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_tips, container, false)
        if (champion == null) {
            Toast.makeText(activity, "Unable to load champion.", Toast.LENGTH_SHORT).show()
            return view
        }
        val tvTips = view.findViewById<View>(R.id.tvTips) as TextView
        var content = ""
        val allyTips = champion!!.allytips
        if (allyTips != null && allyTips.size > 0) {
            content += "<h3>Ally Tips</h3>"
            for (i in allyTips.indices) {
                content += "<p> <b>" + (i + 1) + ".</b> " + allyTips[i] + "</p>"
            }
        }
        val enemyTips = champion!!.enemytips
        if (enemyTips != null && enemyTips.size > 0) {
            content += "<h3>Enemy Tips</h3>"
            for (i in enemyTips.indices) {
                content += "<p> <b>" + (i + 1) + ".</b> " + enemyTips[i] + "</p>"
            }
        }
        tvTips.text = Html.fromHtml(content)
        return view
    }

    fun onButtonPressed(uri: Uri?) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener =
            if (context is OnFragmentInteractionListener) {
                context
            } else {
                throw RuntimeException(
                    context.toString()
                            + " must implement OnWallRecentFragmentInteractionListener"
                )
            }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri?)
    }

    companion object {
        private const val CHAMP = "champ"
        fun newInstance(champ: Champion?): TipsFragment {
            val fragment = TipsFragment()
            val args = Bundle()
            args.putSerializable(CHAMP, champ)
            fragment.arguments = args
            return fragment
        }
    }
}