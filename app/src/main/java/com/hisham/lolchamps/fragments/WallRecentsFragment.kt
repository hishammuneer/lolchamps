package com.hisham.lolchamps.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.hisham.lolchamps.ImageActivity
import com.hisham.lolchamps.compose.theme.LolTheme
import com.hisham.lolchamps.fragments.WallRecentsFragment.OnWallRecentFragmentInteractionListener
import com.hisham.lolchamps.fragments.compose.WallpaperUi
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.model.WallContainerModel
import com.hisham.lolchamps.model.WallItemModel
import com.hisham.lolchamps.model.wallpaper.Asset
import com.hisham.lolchamps.retrofit.LolRetrofit.wallpaperService
import com.hisham.lolchamps.source.DataRepository
import com.hisham.lolchamps.utils.ConstUrl
import kotlinx.coroutines.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OnWallRecentFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [WallRecentsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WallRecentsFragment : Fragment() {
    private var mListener: OnWallRecentFragmentInteractionListener? = null
    private var fileName: String? = null
    private var title: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            fileName = requireArguments().getString(FILE_NAME)
            title = requireArguments().getString(TITLE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        val viewCompose = ComposeView(requireContext())
        viewCompose.setContent {
            val assetList = listLiveData.observeAsState()
            LolTheme(darkTheme = false) {
                assetList.value?.let { it1 ->
                    WallpaperUi(assetList = it1) { asset ->
                        onImageClick(asset)
                    }
                }
            }
        }

        lifecycleScope.launch {
            val assetList = withContext(Dispatchers.IO) {
                val dataRepository = DataRepository(
                    wallpaperService = wallpaperService,
                    prefs = AppSharedPrefs.getInstance(requireActivity())
                )

                val wallPaperList = dataRepository.getWallpapers().data?.assets
                wallPaperList
            }
            assetList?.let {
                listLiveData.value = it
            }
        }

        return viewCompose
    }

    // keeping a live data to update the UI
    private val listLiveData = MutableLiveData<List<Asset>>()

    private fun onImageClick(wallItemModel: Asset) {
        val thumbLink = ConstUrl.BASE_URL_WALL + wallItemModel.thumbnailUrl
        val fullLink = ConstUrl.BASE_URL_WALL + wallItemModel.url
        val size = wallItemModel.size
        val intent = Intent(activity, ImageActivity::class.java)
        val bundle = Bundle()
        val containerModel = WallContainerModel()
        val itemModel = WallItemModel()
        itemModel.thumbLink = thumbLink
        itemModel.link = fullLink
        itemModel.size = size
        val list: MutableList<WallItemModel> = ArrayList()
        list.add(itemModel)
        containerModel.data = list
        bundle.putSerializable("wallContainer", containerModel)
        bundle.putString("title", title)
        startActivity(intent.putExtras(bundle))
    }

    private var typingJob: Job? = null
    fun sortList(patternActual: String) {
        typingJob?.cancel()
        typingJob = lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                delay(400)
                val dataRepository = DataRepository(
                    wallpaperService = wallpaperService,
                    prefs = AppSharedPrefs.getInstance(requireActivity())
                )

                val data = dataRepository.getWallpapers().data
                val wallPaperList = data?.assets

                val filteredList = wallPaperList?.filter {
                    val translationId =
                        it.nameTranslateId?.toLowerCase(Locale.ROOT) ?: UUID.randomUUID().toString()
                    val actualName = data.locale?.translations?.enUS?.get(translationId)
                        ?.toLowerCase(Locale.ROOT) ?: UUID.randomUUID().toString()
                    Log.d("actualName", "sortList: actualName: $actualName")
                    val pattern = patternActual.toLowerCase(Locale.ROOT)
                    val id = it.id ?: UUID.randomUUID().toString()
                    translationId.contains(pattern) ||
                            pattern.contains(translationId) ||
                            actualName.contains(pattern) ||
                            pattern.contains(actualName) ||
                            pattern.contains(id) ||
                            id.contains(pattern)
                }

                listLiveData.postValue(
                    if (filteredList?.isEmpty() == true) wallPaperList
                    else filteredList ?: emptyList()
                )
            }
        }
    }

    // todo below was the code for long press an image
//    viewHolder.imgBack.setOnLongClickListener {
//        if (activity != null) {
//            PermissionHelper().requestPermission(
//                activity!!,
//                object : PermissionHelperListener {
//                    override fun onPermission(granted: Boolean) {
//                        if (granted) {
//                            download(
//                                activity!!,
//                                ConstUrl.BASE_URL_WALL + (wallPaperModel.assets?.get(
//                                    position
//                                )?.url
//                                    ?: ""),
//                                null
//                            )
//                            Toast.makeText(
//                                context,
//                                R.string.downloading_check_notifications,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
//                    }
//                },
//                recyclerView
//            )
//        }
//        true
//    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is OnWallRecentFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                        + " must implement OnWallRecentFragmentInteractionListener"
            )
        }
        mListener?.WallFragRunning()
    }

    override fun onDetach() {
        super.onDetach()
        mListener?.WallFragClosed()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnWallRecentFragmentInteractionListener {
        fun WallFragRunning()
        fun WallFragClosed()
    }

    companion object {
        private const val FILE_NAME = "filename"
        private const val TITLE = "title"
        fun newInstance(fileName: String?, title: String?): WallRecentsFragment {
            val fragment = WallRecentsFragment()
            val args = Bundle()
            args.putString(FILE_NAME, fileName)
            args.putString(TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }
}