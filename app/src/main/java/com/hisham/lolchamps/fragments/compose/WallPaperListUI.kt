package com.hisham.lolchamps.fragments.compose

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.hisham.lolchamps.compose.colors.GoldTransparent
import com.hisham.lolchamps.model.wallpaper.Asset
import com.hisham.lolchamps.utils.ConstUrl
import dev.chrisbanes.accompanist.coil.CoilImage

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun WallpaperUi(assetList: List<Asset>, onClick: (model: Asset) -> Unit) {
    Box {
        LazyVerticalGrid(cells = GridCells.Fixed(3), content = {
            items(assetList) { asset ->
                LolImage(asset = asset, onClick)
            }
        })
        Box(contentAlignment = Alignment.BottomEnd, modifier = Modifier.fillMaxSize()) {
            Text(
                text = "Items: ${assetList.size}",
                style = TextStyle.Default.copy(
                    fontSize = 12.sp,
                    color = Color.White
                ),
                modifier = Modifier
                    .padding(4.dp)
                    .clip(RoundedCornerShape(4.dp))
                    .background(GoldTransparent)
                    .padding(4.dp)
            )
        }
    }
}

@Composable
fun LolImage(
    asset: Asset,
    onClick: (model: Asset) -> Unit
) {
    CoilImage(
        data = ConstUrl.BASE_URL_WALL + asset.thumbnailUrl,
        contentDescription = "image",
        fadeIn = true,
        contentScale = ContentScale.Crop,
        error = {
            Row(
                Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                CircularProgressIndicator(color = Color.Gray)
            }
        },
        loading = {
            Row(
                Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                CircularProgressIndicator(color = Color.Gray)
            }
        },
        modifier = Modifier
            .height(120.dp)
            .padding(2.dp)
            .shadow(1.dp)
            .clip(
                RoundedCornerShape(
                    topStart = 16.dp,
                    topEnd = 4.dp,
                    bottomStart = 4.dp,
                    bottomEnd = 16.dp
                )
            )
            .clickable {
                onClick.invoke(asset)
            },
    )
}