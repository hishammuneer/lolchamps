package com.hisham.lolchamps.helper

import android.content.Context
import android.util.Log
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.hisham.lolchamps.helper.AdHelper

/**
 * Created by Hisham on 28/Oct/2016 - 00:00
 */
class AdHelper(private val context: Context) {
    private var adHelperListener: AdHelperListener? = null
    private var rewardAdHelperListener: RewardAdHelperListener? = null
    private var mInterstitialAd: InterstitialAd? = null
    private var mRewardAd: RewardedVideoAd? = null
    fun setUpRewardAd(): AdHelper {
        mRewardAd = MobileAds.getRewardedVideoAdInstance(context)
        mRewardAd?.loadAd("ca-app-pub-1590664686760927/4188308295", adRequest) // reward video
        mRewardAd?.setRewardedVideoAdListener(object : RewardedVideoAdListener {
            override fun onRewardedVideoAdLoaded() {}
            override fun onRewardedVideoAdOpened() {}
            override fun onRewardedVideoStarted() {}
            override fun onRewardedVideoAdClosed() {
                if (rewardAdHelperListener != null) {
                    rewardAdHelperListener!!.onRewardAdClosed()
                }
            }

            override fun onRewarded(rewardItem: RewardItem) {
                Log.d(
                    TAG,
                    "onRewarded! currency: " + rewardItem.type + "  amount: " + rewardItem.amount
                )
                if (rewardAdHelperListener != null) {
                    rewardAdHelperListener!!.onRewarded(rewardItem)
                }
            }

            override fun onRewardedVideoAdLeftApplication() {}
            override fun onRewardedVideoAdFailedToLoad(i: Int) {}
            override fun onRewardedVideoCompleted() {}
        })
        return this
    }

    fun setAdListener(listener: AdHelperListener?): AdHelper {
        adHelperListener = listener
        return this
    }

    fun setRewardAdListener(listener: RewardAdHelperListener?): AdHelper {
        rewardAdHelperListener = listener
        return this
    }

    fun setUpInterstitialAd(context: Context?): AdHelper {
        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd!!.adUnitId = "ca-app-pub-1590664686760927/6862573094" // lol champs
        mInterstitialAd!!.adListener = object : AdListener() {
            override fun onAdClosed() {
                if (adHelperListener != null) {
                    adHelperListener!!.onAdClosed()
                }
                requestNewInterstitial()
            }
        }
        requestNewInterstitial()
        return this
    }

    val adRequest: AdRequest
        get() = AdRequest.Builder()
            .addTestDevice("5A0CE278C46C002116E4C632B1694DD3")
            .build()

    private fun requestNewInterstitial() {
        mInterstitialAd!!.loadAd(adRequest)
    }

    val isInterstitialAdLoaded: Boolean
        get() = mInterstitialAd!!.isLoaded
    val isRewardAdLoaded: Boolean
        get() = mRewardAd!!.isLoaded

    fun showInterstitialAd(): AdHelper {
        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        }
        return this
    }

    fun showRewardAd(): AdHelper {
        if (mRewardAd!!.isLoaded) {
            mRewardAd!!.show()
        }
        return this
    }

    interface AdHelperListener {
        fun onAdClosed()
    }

    interface RewardAdHelperListener {
        fun onRewardAdClosed()
        fun onRewarded(reward: RewardItem?)
    }

    companion object {
        private val TAG = AdHelper::class.java.simpleName
    }
}