package com.hisham.lolchamps.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.TextUtils
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.utils.AppConstants

/**
 * Created by Hisham on 28/04/16.
 */
class AppSharedPrefs private constructor(context: Context) {
    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    fun saveStringPreference(key: String?, value: String?) {
        val editor = preferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun saveIntPreference(key: String?, value: Int) {
        val editor = preferences.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun saveBooleanPreference(key: String?, value: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun saveLongPreference(key: String?, value: Long) {
        val editor = preferences.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    /**
     * Clear all the user's saved data
     */
    fun clearAllSharedPreferences() {
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

    /**
     * Clear all the user's saved data
     */
    fun clearWallSharedPreferences() {
        val editor = preferences.edit()
        editor.remove(AppConstants.RECENTS)
        editor.remove(AppConstants.FAN_ART)
        editor.remove(AppConstants.POROS)
        editor.apply()
    }

    //Log.d(TAG, "getLastNotificationTime: " + time);
    val lastNotificationTime: String?
        get() {
            var time = preferences.getString(LAST_NOTIFICATION_TIME, null)
            if (TextUtils.isEmpty(time)) {
                time = "2010-07-03 19:54:29"
            }
            //Log.d(TAG, "getLastNotificationTime: " + time);
            return time
        }
    val championsList: String?
        get() = preferences.getString(CHAMP_LIST, null)
    val wallPaperList: String?
        get() = preferences.getString(WALLPAPER_LIST, "")

    fun getChampionByKey(key: String?): String? {
        return preferences.getString(key, "")
    }

    val appSideVersion: Long
        get() = preferences.getLong(APP_SIDE_UPDATE_VERSION, 9)
    val wallPapersVersion: Long
        get() = preferences.getLong(APP_SIDE_WALL_VERSION, 11)

    // default is 10.9.1
    val dataVersion: String?
        get() {
            val dataVersion = preferences.getString(RIOT_DATA_VERSION, null)
            return if (TextUtils.isEmpty(dataVersion)) {
                "10.9.1" // default is 10.9.1
            } else dataVersion
        }

    // filename is something like this public static final String RECENTS = "recents";
    fun getWallLinksString(fileName: String?): String? {
        return preferences.getString(fileName, "")
    }

    val lastSelection: Int
        get() = preferences.getInt("lastSelection", -1)

    companion object {
        const val LAST_NOTIFICATION_TIME = "LAST_NOTIFICATION_TIME"
        const val CHAMP_LIST = "CHAMP_LIST"
        const val WALLPAPER_LIST = "WALLPAPER_LIST"
        const val APP_SIDE_UPDATE_VERSION = "APP_SIDE_UPDATE_VERSION"
        const val RIOT_DATA_VERSION = "RIOT_DATA_VERSION"
        const val WALL_FILE_RECENT = "WALL_FILE_RECENT"
        const val APP_SIDE_WALL_VERSION = "APP_SIDE_WALL_VERSION"
        private var ourInstance: AppSharedPrefs? = null
        private val TAG = AppSharedPrefs::class.java.simpleName
        fun getInstance(context: Context): AppSharedPrefs {
            if (ourInstance == null) ourInstance = AppSharedPrefs(context)
            return ourInstance!!
        }
    }

}