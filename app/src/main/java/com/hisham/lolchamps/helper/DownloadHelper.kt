package com.hisham.lolchamps.helper

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.R


object DownloadHelper {

    @JvmOverloads
    fun download(context: Context, url: String, size: Long? = null) {
        val mimeType = getMimeType(url)
        val fileName = URLUtil.guessFileName(url, null, mimeType)

        // Create request for android download manager
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
        val request = DownloadManager.Request(Uri.parse(url))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)

        // set title and description
        request.setTitle(context.getString(R.string.app_name) + " - " + fileName)
        val description = if (size != null) {
            "Downloading $fileName (${Globals.getFileSize(size)})"
        } else {
            "Downloading $fileName"
        }
        request.setDescription(description)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

        //set the local destination for download file to a path within the application's external files directory
        request.setMimeType(mimeType)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
        downloadManager?.enqueue(request)
    }

    private fun getMimeType(url: String) = MimeTypeMap.getFileExtensionFromUrl(url)?.let {
        MimeTypeMap.getSingleton().getMimeTypeFromExtension(it)
    } ?: "image/*"

}