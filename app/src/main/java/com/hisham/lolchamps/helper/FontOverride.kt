package com.hisham.lolchamps.helper

import android.content.Context
import android.graphics.Typeface

/**
 * How to use:
 *
 *
 * 1. Put your font file in assets. In your Application Class write following:
 *
 * @Override public void onCreate() {
 * super.onCreate();
 * FontOverride.setDefaultFont(this, "DEFAULT", "AvenirRoman.otf");
 * FontOverride.setDefaultFont(this, "SANS_SERIF", "AvenirRoman.otf");
 * }
 *
 *
 * 2. In your styles.xml
 *
 *
 * <style name="AppTheme.CustomFontTheme">
<item name="android:typeface">sans</item>
</style>
 *
 *
 * 3. In your manifest file apply this style to application or activity
 */
object FontOverride {
    fun setDefaultFont(
        context: Context,
        staticTypefaceFieldName: String?, fontAssetName: String?
    ) {
        val regular = Typeface.createFromAsset(
            context.assets,
            fontAssetName
        )
        replaceFont(staticTypefaceFieldName, regular)
    }

    internal fun replaceFont(
        staticTypefaceFieldName: String?,
        newTypeface: Typeface?
    ) {
        try {
            val staticField = Typeface::class.java
                .getDeclaredField(staticTypefaceFieldName)
            staticField.isAccessible = true
            staticField[null] = newTypeface
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }
}