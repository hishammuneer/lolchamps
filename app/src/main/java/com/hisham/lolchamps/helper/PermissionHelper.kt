package com.hisham.lolchamps.helper

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest

interface PermissionHelperListener {
    fun onPermission(granted: Boolean)
}

class PermissionHelper {
    @JvmOverloads
    fun requestPermission(
        activity: Activity,
        listener: PermissionHelperListener,
        view: View? = null,
        snackbarText: String = "Storage permission required to download images.",
        actionText: String = "Open Settings",
        openSettingsOnDenial: Boolean = true
    ) {
        Dexter.withContext(activity)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : com.karumi.dexter.listener.single.PermissionListener {

                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    listener.onPermission(true)
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    listener.onPermission(false)
                    showSnackBar(openSettingsOnDenial, view, snackbarText, actionText, activity)
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    listener.onPermission(false)
                    showSnackBar(openSettingsOnDenial, view, snackbarText, actionText, activity)
                }
            }).check()
    }

    private fun showSnackBar(
        openSettingsOnDenial: Boolean,
        view: View?,
        snackbarText: String,
        actionText: String,
        activity: Activity
    ) {
        if (openSettingsOnDenial && view != null) {
            Snackbar.make(
                view,
                snackbarText,
                Snackbar.LENGTH_LONG
            ).setAction(actionText) {
                openPermissionSettings(activity)
            }.show()
        }
    }

    private fun openPermissionSettings(context: Context) {
        val i = Intent()
        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.data = Uri.parse("package:" + context.packageName)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        context.startActivity(i)
    }
}