package com.hisham.lolchamps.helper

import java.io.Serializable

/**
 * Created by Hisham on 02/Apr/2017 - 16:00
 */
class RegionStory : Serializable {
    var name: String? = null
    var story: String? = null
    var crestIcon: String? = null
    var coverImageUrl: String? = null
    var champions: List<StoryChampions>? = null
    var extras: List<StoryExtras>? = null
}