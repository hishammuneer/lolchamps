package com.hisham.lolchamps.helper

import java.io.Serializable

/**
 * Created by Hisham on 02/Apr/2017 - 15:59
 */
class RegionStoryHelper : Serializable {
    constructor()
    constructor(regions: List<RegionStory>?) {
        this.regions = regions
    }

    var regions: List<RegionStory>? = null
}