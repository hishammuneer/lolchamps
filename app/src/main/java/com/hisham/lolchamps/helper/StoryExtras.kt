package com.hisham.lolchamps.helper

import java.io.Serializable

/**
 * Created by Hisham on 05/Apr/2017 - 15:33
 */
class StoryExtras : Serializable {
    var url: String? = null
    var title: String? = null
    var description: String? = null
}