package com.hisham.lolchamps.interfaces

import net.rithms.riot.api.endpoints.static_data.dto.Champion

/**
 * Created by Hisham on 16/Mar/2017 - 02:15
 */
interface IChampion {
    fun onChampionLoaded(champion: Champion?)
}