package com.hisham.lolchamps.interfaces

interface IDataVersion {
    fun onLoad(version: String?)
}