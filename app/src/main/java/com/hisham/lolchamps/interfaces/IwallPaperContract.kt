package com.hisham.lolchamps.interfaces

import com.hisham.lolchamps.model.wallpaper.WallPaperModel

/**
 * Created by Hisham on 14/Aug/2018 - 17:26
 */
interface IwallPaperContract {
    fun onWallpaperDataLoaded(wallPaperModel: WallPaperModel?)
}