package com.hisham.lolchamps.model

import android.text.TextUtils
import java.io.Serializable

/**
 * Created by Hisham on 11/Sep/2017 - 16:25
 */
class FileDownloadModel : Serializable {
    enum class Status {
        DOWNLOADING, DOWNLOADED, UNZIPPING, UNZIPPED, CANCELLED, ERROR
    }

    var link: String? = null
        private set
    var callBackIntent: String? = null
        private set
    var progress = -1
        private set

    /**
     * status like unzipping, downloading... etc
     */
    var status: Status? = null
        private set

    fun setStatus(status: Status?): FileDownloadModel {
        this.status = status
        return this
    }

    fun setProgress(progress: Int): FileDownloadModel {
        this.progress = progress
        return this
    }

    fun setLink(link: String?): FileDownloadModel {
        this.link = link
        return this
    }

    fun setCallBackIntent(callBackIntent: String?): FileDownloadModel {
        this.callBackIntent = callBackIntent
        return this
    }

    override fun equals(obj: Any?): Boolean {
        val model = obj as FileDownloadModel?
        return if (!TextUtils.isEmpty(link) && link.equals(model!!.link, ignoreCase = true)) {
            true
        } else super.equals(obj)
    }
}