package com.hisham.lolchamps.model

import java.io.Serializable

/**
 * Created by Hisham on 21/Sep/2017 - 14:42
 */
class WallContainerModel : Serializable {
    var version = 0
        private set
    var data: List<WallItemModel>? = null
}