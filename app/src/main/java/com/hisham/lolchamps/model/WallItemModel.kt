package com.hisham.lolchamps.model

import java.io.Serializable

/**
 * Created by Hisham on 21/Sep/2017 - 14:42
 */
class WallItemModel : Serializable {
    var title: String? = null
        private set
    var thumbLink: String? = null
    var link: String? = null
    var size: Int? = null
    var thumbnailSize: Int? = null
        private set

    fun setSize(size: Int?): WallItemModel {
        this.size = size
        return this
    }

    fun setThumbnailSize(thumbnailSize: Int?): WallItemModel {
        this.thumbnailSize = thumbnailSize
        return this
    }

    fun setLink(link: String?): WallItemModel {
        this.link = link
        return this
    }

    fun setTitle(title: String?): WallItemModel {
        this.title = title
        return this
    }

    fun setThumbLink(thumbLink: String?): WallItemModel {
        this.thumbLink = thumbLink
        return this
    }
}