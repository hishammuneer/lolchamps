package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Asset : Serializable {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("dateAdded")
    @Expose
    var dateAdded: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    @SerializedName("type")
    @Expose
    var type: String? = null

    @SerializedName("size")
    @Expose
    var size: Int? = null

    @SerializedName("color")
    @Expose
    var color: String? = null

    @SerializedName("thumbnailUrl")
    @Expose
    var thumbnailUrl: String? = null

    @SerializedName("thumbnailSize")
    @Expose
    var thumbnailSize: Int? = null

    @SerializedName("nameTranslateId")
    @Expose
    var nameTranslateId: String? = null
    fun withId(id: String?): Asset {
        this.id = id
        return this
    }

    fun withDateAdded(dateAdded: String?): Asset {
        this.dateAdded = dateAdded
        return this
    }

    fun withUrl(url: String?): Asset {
        this.url = url
        return this
    }

    fun withTags(tags: List<String>?): Asset {
        this.tags = tags
        return this
    }

    fun withType(type: String?): Asset {
        this.type = type
        return this
    }

    fun withSize(size: Int?): Asset {
        this.size = size
        return this
    }

    fun withColor(color: String?): Asset {
        this.color = color
        return this
    }

    fun withThumbnailUrl(thumbnailUrl: String?): Asset {
        this.thumbnailUrl = thumbnailUrl
        return this
    }

    fun withThumbnailSize(thumbnailSize: Int?): Asset {
        this.thumbnailSize = thumbnailSize
        return this
    }

    fun withNameTranslateId(nameTranslateId: String?): Asset {
        this.nameTranslateId = nameTranslateId
        return this
    }

    companion object {
        private const val serialVersionUID = 5743300890768347886L
    }
}