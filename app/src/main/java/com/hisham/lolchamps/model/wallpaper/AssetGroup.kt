package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AssetGroup : Serializable {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("previewUrl")
    @Expose
    var previewUrl: String? = null

    @SerializedName("dateAdded")
    @Expose
    var dateAdded: String? = null

    @SerializedName("assets")
    @Expose
    var assets: List<String>? = null

    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    @SerializedName("nameTranslateId")
    @Expose
    var nameTranslateId: String? = null

    @SerializedName("previewThumbnailUrl")
    @Expose
    var previewThumbnailUrl: String? = null

    @SerializedName("previewThumbnailSize")
    @Expose
    var previewThumbnailSize: Int? = null
    fun withId(id: String?): AssetGroup {
        this.id = id
        return this
    }

    fun withPreviewUrl(previewUrl: String?): AssetGroup {
        this.previewUrl = previewUrl
        return this
    }

    fun withDateAdded(dateAdded: String?): AssetGroup {
        this.dateAdded = dateAdded
        return this
    }

    fun withAssets(assets: List<String>?): AssetGroup {
        this.assets = assets
        return this
    }

    fun withTags(tags: List<String>?): AssetGroup {
        this.tags = tags
        return this
    }

    fun withNameTranslateId(nameTranslateId: String?): AssetGroup {
        this.nameTranslateId = nameTranslateId
        return this
    }

    fun withPreviewThumbnailUrl(previewThumbnailUrl: String?): AssetGroup {
        this.previewThumbnailUrl = previewThumbnailUrl
        return this
    }

    fun withPreviewThumbnailSize(previewThumbnailSize: Int?): AssetGroup {
        this.previewThumbnailSize = previewThumbnailSize
        return this
    }

    companion object {
        private const val serialVersionUID = 6188771189081305145L
    }
}