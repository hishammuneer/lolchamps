package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AssetGroupType : Serializable {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("showAlphabet")
    @Expose
    var showAlphabet: Boolean? = null

    @SerializedName("nameTranslateId")
    @Expose
    var nameTranslateId: String? = null

    @SerializedName("hidden")
    @Expose
    var hidden: Boolean? = null
    fun withId(id: String?): AssetGroupType {
        this.id = id
        return this
    }

    fun withShowAlphabet(showAlphabet: Boolean?): AssetGroupType {
        this.showAlphabet = showAlphabet
        return this
    }

    fun withNameTranslateId(nameTranslateId: String?): AssetGroupType {
        this.nameTranslateId = nameTranslateId
        return this
    }

    fun withHidden(hidden: Boolean?): AssetGroupType {
        this.hidden = hidden
        return this
    }

    companion object {
        private const val serialVersionUID = 5690972978364181874L
    }
}