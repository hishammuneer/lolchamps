package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AssetType : Serializable {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("nameTranslateId")
    @Expose
    var nameTranslateId: String? = null
    fun withId(id: String?): AssetType {
        this.id = id
        return this
    }

    fun withNameTranslateId(nameTranslateId: String?): AssetType {
        this.nameTranslateId = nameTranslateId
        return this
    }

    companion object {
        private const val serialVersionUID = 5975661660296849078L
    }
}