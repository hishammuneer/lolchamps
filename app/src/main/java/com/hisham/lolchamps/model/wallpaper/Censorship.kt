package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Censorship : Serializable {
    @SerializedName("tr_TR")
    @Expose
    var trTR: List<String>? = null

    @SerializedName("ru_RU")
    @Expose
    var ruRU: List<String>? = null

    @SerializedName("ko_KR")
    @Expose
    var koKR: List<String>? = null
    fun withTrTR(trTR: List<String>?): Censorship {
        this.trTR = trTR
        return this
    }

    fun withRuRU(ruRU: List<String>?): Censorship {
        this.ruRU = ruRU
        return this
    }

    fun withKoKR(koKR: List<String>?): Censorship {
        this.koKR = koKR
        return this
    }

    companion object {
        private const val serialVersionUID = 7556075330434875054L
    }
}