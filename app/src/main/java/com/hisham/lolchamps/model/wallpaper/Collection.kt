package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Collection : Serializable {
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("coverAssetId")
    @Expose
    var coverAssetId: String? = null

    @SerializedName("categories")
    @Expose
    var categories: List<String>? = null

    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    @SerializedName("nameTranslateId")
    @Expose
    var nameTranslateId: String? = null

    @SerializedName("positionX")
    @Expose
    var positionX: String? = null

    @SerializedName("assets")
    @Expose
    var assets: List<String>? = null
    fun withId(id: String?): Collection {
        this.id = id
        return this
    }

    fun withCoverAssetId(coverAssetId: String?): Collection {
        this.coverAssetId = coverAssetId
        return this
    }

    fun withCategories(categories: List<String>?): Collection {
        this.categories = categories
        return this
    }

    fun withTags(tags: List<String>?): Collection {
        this.tags = tags
        return this
    }

    fun withNameTranslateId(nameTranslateId: String?): Collection {
        this.nameTranslateId = nameTranslateId
        return this
    }

    fun withPositionX(positionX: String?): Collection {
        this.positionX = positionX
        return this
    }

    fun withAssets(assets: List<String>?): Collection {
        this.assets = assets
        return this
    }

    companion object {
        private const val serialVersionUID = -7888931569293211166L
    }
}