package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Collections : Serializable {
    @SerializedName("featured")
    @Expose
    var featured: List<String>? = null

    @SerializedName("collections")
    @Expose
    var collections: List<Collection>? = null
    fun withFeatured(featured: List<String>?): Collections {
        this.featured = featured
        return this
    }

    fun withCollections(collections: List<Collection>?): Collections {
        this.collections = collections
        return this
    }

    companion object {
        private const val serialVersionUID = 4278592987575240450L
    }
}