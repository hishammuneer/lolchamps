package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DefaultProfile : Serializable {
    @SerializedName("animationDuration")
    @Expose
    var animationDuration: Int? = null

    @SerializedName("imageDuration")
    @Expose
    var imageDuration: Int? = null

    @SerializedName("displayMirroring")
    @Expose
    var displayMirroring: Boolean? = null

    @SerializedName("assets")
    @Expose
    var assets: List<String>? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null
    fun withAnimationDuration(animationDuration: Int?): DefaultProfile {
        this.animationDuration = animationDuration
        return this
    }

    fun withImageDuration(imageDuration: Int?): DefaultProfile {
        this.imageDuration = imageDuration
        return this
    }

    fun withDisplayMirroring(displayMirroring: Boolean?): DefaultProfile {
        this.displayMirroring = displayMirroring
        return this
    }

    fun withAssets(assets: List<String>?): DefaultProfile {
        this.assets = assets
        return this
    }

    fun withId(id: Int?): DefaultProfile {
        this.id = id
        return this
    }

    companion object {
        private const val serialVersionUID = 4619504359655579350L
    }
}