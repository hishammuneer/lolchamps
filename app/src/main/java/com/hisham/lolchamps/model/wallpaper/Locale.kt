package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Locale : Serializable {
    @SerializedName("fallbackLocale")
    @Expose
    var fallbackLocale: String? = null

    @SerializedName("locales")
    @Expose
    var locales: List<String>? = null

    @SerializedName("translationExceptions")
    @Expose
    var translationExceptions: TranslationExceptions? = null

    @SerializedName("translations")
    @Expose
    var translations: Translations? = null
    fun withFallbackLocale(fallbackLocale: String?): Locale {
        this.fallbackLocale = fallbackLocale
        return this
    }

    fun withLocales(locales: List<String>?): Locale {
        this.locales = locales
        return this
    }

    fun withTranslationExceptions(translationExceptions: TranslationExceptions?): Locale {
        this.translationExceptions = translationExceptions
        return this
    }

    fun withTranslations(translations: Translations?): Locale {
        this.translations = translations
        return this
    }

    companion object {
        private const val serialVersionUID = 8642510488597819856L
    }
}