package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TrTR : Serializable {
    @SerializedName("lss_app_content_champion-.+")
    @Expose
    var lssAppContentChampion: String? = null

    @SerializedName("lss_app_content_runeterra")
    @Expose
    var lssAppContentRuneterra: String? = null

    @SerializedName("lss_app_content_runeterra-bilgewater")
    @Expose
    var lssAppContentRuneterraBilgewater: String? = null

    @SerializedName("lss_app_content_runeterra-freljord")
    @Expose
    var lssAppContentRuneterraFreljord: String? = null

    @SerializedName("lss_app_content_runeterra-ionia")
    @Expose
    var lssAppContentRuneterraIonia: String? = null

    @SerializedName("lss_app_content_runeterra-shurima")
    @Expose
    var lssAppContentRuneterraShurima: String? = null
    fun withLssAppContentChampion(lssAppContentChampion: String?): TrTR {
        this.lssAppContentChampion = lssAppContentChampion
        return this
    }

    fun withLssAppContentRuneterra(lssAppContentRuneterra: String?): TrTR {
        this.lssAppContentRuneterra = lssAppContentRuneterra
        return this
    }

    fun withLssAppContentRuneterraBilgewater(lssAppContentRuneterraBilgewater: String?): TrTR {
        this.lssAppContentRuneterraBilgewater = lssAppContentRuneterraBilgewater
        return this
    }

    fun withLssAppContentRuneterraFreljord(lssAppContentRuneterraFreljord: String?): TrTR {
        this.lssAppContentRuneterraFreljord = lssAppContentRuneterraFreljord
        return this
    }

    fun withLssAppContentRuneterraIonia(lssAppContentRuneterraIonia: String?): TrTR {
        this.lssAppContentRuneterraIonia = lssAppContentRuneterraIonia
        return this
    }

    fun withLssAppContentRuneterraShurima(lssAppContentRuneterraShurima: String?): TrTR {
        this.lssAppContentRuneterraShurima = lssAppContentRuneterraShurima
        return this
    }

    companion object {
        private const val serialVersionUID = 7127355180416991275L
    }
}