package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TranslationExceptions : Serializable {
    @SerializedName("tr_TR")
    @Expose
    var trTR: TrTR? = null
    fun withTrTR(trTR: TrTR?): TranslationExceptions {
        this.trTR = trTR
        return this
    }

    companion object {
        private const val serialVersionUID = 8286174216064497128L
    }
}