package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Translations : Serializable {
    @SerializedName("en_US")
    @Expose
    var enUS: Map<String, String>? = null

    companion object {
        private const val serialVersionUID = -1735256919521673347L
    }
}