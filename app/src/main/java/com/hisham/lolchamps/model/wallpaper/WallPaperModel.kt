package com.hisham.lolchamps.model.wallpaper

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WallPaperModel : Serializable {
    @SerializedName("assetGroupTypes")
    @Expose
    var assetGroupTypes: List<AssetGroupType>? = null

    @SerializedName("assetTypes")
    @Expose
    var assetTypes: List<AssetType>? = null

    @SerializedName("assetGroups")
    @Expose
    var assetGroups: List<AssetGroup>? = null

    @SerializedName("assets")
    @Expose
    var assets: List<Asset>? = null

    @SerializedName("defaultProfile")
    @Expose
    var defaultProfile: DefaultProfile? = null

    @SerializedName("collections")
    @Expose
    var collections: Collections? = null

    @SerializedName("censorship")
    @Expose
    var censorship: Censorship? = null

    @SerializedName("locale")
    @Expose
    var locale: Locale? = null

    @SerializedName("versionDate")
    @Expose
    var versionDate: String? = null
    fun withAssetGroupTypes(assetGroupTypes: List<AssetGroupType>?): WallPaperModel {
        this.assetGroupTypes = assetGroupTypes
        return this
    }

    fun withAssetTypes(assetTypes: List<AssetType>?): WallPaperModel {
        this.assetTypes = assetTypes
        return this
    }

    fun withAssetGroups(assetGroups: List<AssetGroup>?): WallPaperModel {
        this.assetGroups = assetGroups
        return this
    }

    fun withAssets(assets: List<Asset>?): WallPaperModel {
        this.assets = assets
        return this
    }

    fun withDefaultProfile(defaultProfile: DefaultProfile?): WallPaperModel {
        this.defaultProfile = defaultProfile
        return this
    }

    fun withCollections(collections: Collections?): WallPaperModel {
        this.collections = collections
        return this
    }

    fun withCensorship(censorship: Censorship?): WallPaperModel {
        this.censorship = censorship
        return this
    }

    fun withLocale(locale: Locale?): WallPaperModel {
        this.locale = locale
        return this
    }

    fun withVersionDate(versionDate: String?): WallPaperModel {
        this.versionDate = versionDate
        return this
    }

    companion object {
        private const val serialVersionUID = 6731515103928475077L
    }
}