package com.hisham.lolchamps.retrofit

import net.rithms.riot.api.endpoints.static_data.dto.ChampionList
import retrofit2.http.GET
import retrofit2.http.Path

interface LolChampsApi {

    @GET("/api/versions.json")
    suspend fun getLatestChampVersion(): List<String>

    @GET("/cdn/{version}/data/en_US/champion.json")
    suspend fun getChampions(@Path("version") version: String): ChampionList

}