package com.hisham.lolchamps.retrofit

import com.hisham.lolchamps.utils.ConstUrl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object LolRetrofit {

    private val retrofitChampApi = Retrofit.Builder()
        .baseUrl(ConstUrl.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val retrofitWallpaperApi = Retrofit.Builder()
        .baseUrl(ConstUrl.BASE_WALLPAPERS_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val champService: LolChampsApi = retrofitChampApi.create(LolChampsApi::class.java)
    val wallpaperService: LolWallpaperApi = retrofitWallpaperApi.create(LolWallpaperApi::class.java)
}