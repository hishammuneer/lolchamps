package com.hisham.lolchamps.retrofit

import com.hisham.lolchamps.model.wallpaper.WallPaperModel
import retrofit2.http.GET

interface LolWallpaperApi {

    @GET("http://screensavers.riotgames.com/v2/latest/content/data.json")
    suspend fun getWallpaperData(): WallPaperModel // todo 1.47 mb data is being downloaded from this api, need to setup a caching mechanism

}