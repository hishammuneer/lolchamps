package com.hisham.lolchamps.server

import android.app.Activity
import android.text.TextUtils
import android.util.Log
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.hisham.lolchamps.Globals
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.interfaces.IChampion
import com.hisham.lolchamps.model.WallContainerModel
import com.hisham.lolchamps.utils.ConstUrl
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

/**
 * Created by Hisham on 15/Mar/2017 - 00:56
 */
class ServerHit private constructor() {
    fun getChampionByKey(activity: Activity, champKey: String, callback: IChampion) {
        val championByKey: String? =
            AppSharedPrefs.getInstance(activity).getChampionByKey(champKey)
        if (!TextUtils.isEmpty(championByKey)) {
            val champList = Gson().fromJson(championByKey, ChampionList::class.java)
            callback.onChampionLoaded(champList.data[champKey])
            return
        }
        if (!Globals.isOnline(activity)) {
            callback.onChampionLoaded(null)
            return
        }
        val url =
            ConstUrl.BASE_URL + "cdn/" + AppSharedPrefs.getInstance(activity).dataVersion + "/data/en_US/champion/" + champKey + ".json"
        val requestQueue = Volley.newRequestQueue(activity)
        val stringRequest = StringRequest(Request.Method.GET, url, { response ->
            Log.d(TAG, "onResponse: $response")
            AppSharedPrefs.getInstance(activity)
                .saveStringPreference(champKey, response)
            val champList = Gson().fromJson(response, ChampionList::class.java)
            callback.onChampionLoaded(champList.data[champKey])
        }) { error ->
            callback.onChampionLoaded(null)
            Log.e(TAG, "error: $error")
        }
        stringRequest.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue.add(stringRequest)
    }


    interface StorageListener {
        fun onResponse(
            success: Boolean,
            serverLocal: Boolean,
            wallContainerModel: WallContainerModel?
        )
    }

    companion object {
        val instance = ServerHit()
        private val TAG = ServerHit::class.java.simpleName
    }
}