package com.hisham.lolchamps.source

object AppCache {

    private val map: MutableMap<String, Cache<*>> = mutableMapOf()

    fun get(key: String): Cache<*>? {
        return (map[key])
    }

    fun put(key: String, value: Any) {
        map[key] = CacheItem(value)
    }

}