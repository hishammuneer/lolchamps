package com.hisham.lolchamps.source

interface Cache<T> {
    var cache: T
}