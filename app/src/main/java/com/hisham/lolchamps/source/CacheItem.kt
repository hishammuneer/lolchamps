package com.hisham.lolchamps.source

data class CacheItem<T>(private var dataItem: T) : Cache<T> {
    override var cache: T
        get() = dataItem
        set(value) {
            dataItem = value
        }
}