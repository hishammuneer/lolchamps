package com.hisham.lolchamps.source

import com.google.gson.Gson
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.model.wallpaper.WallPaperModel
import com.hisham.lolchamps.retrofit.LolChampsApi
import com.hisham.lolchamps.retrofit.LolWallpaperApi
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

/**
 * The purpose of this class is to act as a single source of truth
 * This will fetch the required data
 */
class DataRepository(
    private val champService: LolChampsApi? = null,
    private val wallpaperService: LolWallpaperApi? = null,
    private val prefs: AppSharedPrefs? = null
) {

    suspend fun getLatestVersion(): LoLResponse<String> {
        // todo check for no internet scenario or network failure or similar and how this behaves

        val response = try {
            val latestChampVersion = champService?.getLatestChampVersion()
            val version = AppCache.get("version")?.cache as? String ?: latestChampVersion
                    ?.first()
            AppCache.put("version", CacheItem(version))
            LoLResponse(data = version)
        } catch (e: Exception) {
            e.printStackTrace()
            ErrorHandling.handleError(e)
        }
        return response
    }

    suspend fun getChampionsList(): LoLResponse<ChampionList?> {

        val champions: LoLResponse<ChampionList?> = LoLResponse(
            data = (AppCache.get("champions")?.cache as? ChampionList)
                ?: Gson().fromJson(prefs?.championsList, ChampionList::class.java) ?: return try {
                    val champions = getLatestVersion().data?.let { champService?.getChampions(it) }
                    AppCache.put("champions", CacheItem(champions))
                    prefs?.saveStringPreference(AppSharedPrefs.CHAMP_LIST, Gson().toJson(champions))
                    LoLResponse(data = champions)
                } catch (e: Exception) {
                    ErrorHandling.handleError(e)
                }
        )
        return champions
    }

    fun getChampion() {

    }

    suspend fun getWallpapers(): LoLResponse<WallPaperModel> {
        val wallPaperModel =
            LoLResponse(data = AppCache.get("wallpapers")?.cache as? WallPaperModel
                ?: Gson().fromJson(prefs?.wallPaperList, WallPaperModel::class.java)
                ?: return try {
                    val wallpaperData = wallpaperService?.getWallpaperData()
                    AppCache.put("wallpapers", CacheItem(wallpaperData))
                    LoLResponse(data = wallpaperData)
                } catch (e: Exception) {
                    ErrorHandling.handleError(e)
                })
        return wallPaperModel
    }

}