package com.hisham.lolchamps.source

import com.hisham.lolchamps.R
import java.net.UnknownHostException

object ErrorHandling {
    fun <T> handleError(e: Exception): LoLResponse<T> {
        return when (e) {
            is UnknownHostException -> {
                LoLResponse(error = R.string.unable_to_connect)
            }
            else -> {
                LoLResponse(error = R.string.unknown_error)
            }
        }
    }
}

data class LoLResponse<T>(val error: Int? = null, val data: T? = null)