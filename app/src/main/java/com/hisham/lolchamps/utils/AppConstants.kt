package com.hisham.lolchamps.utils

/**
 * Created by Hisham on 20/Sep/2017 - 13:53
 */
object AppConstants {
    /**
     * All variables below must be referenced in #AppSharedPrefs.clearWallSharedPreferences() for removal on version update
     */
    const val RECENTS =
        "recents_json.txt" // file with the exact same name should be uploaded on server: https://console.firebase.google.com/project/lolchamps-11338/storage/lolchamps-11338.appspot.com/files
    const val FAN_ART = "fan_art_json.txt"
    const val POROS = "poros_json.txt"
}