package com.hisham.lolchamps.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build

object CompatibilityUtil {
    /** Get the current Android API level.  */
    val sdkVersion: Int
        get() = Build.VERSION.SDK_INT

    /** Determine if the device is running API level 8 or higher.  */
    val isFroyo: Boolean
        get() = sdkVersion >= Build.VERSION_CODES.FROYO

    /** Determine if the device is running API level 11 or higher.  */
    val isHoneycomb: Boolean
        get() = sdkVersion >= Build.VERSION_CODES.HONEYCOMB

    /**
     * Determine if the device is a tablet (i.e. it has a large screen).
     *
     * @param context The calling context.
     */
    fun isTablet(context: Context): Boolean {
        return ((context.resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }

    /**
     * Determine if the device is a HoneyComb tablet.
     *
     * @param context The calling context.
     */
    fun isHoneycombTablet(context: Context): Boolean {
        return isHoneycomb && isTablet(context)
    }

    /**
     * Determine if the device is in landscape mode
     *
     * @param context The calling context.
     */
    fun isInLandScapce(context: Context): Boolean {
        val ot = context.resources.configuration.orientation
        return ot == Configuration.ORIENTATION_LANDSCAPE
    }
}