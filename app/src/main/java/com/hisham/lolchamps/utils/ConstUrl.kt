package com.hisham.lolchamps.utils

/**
 * Created by Hisham on 22/Mar/2017 - 14:02
 */
object ConstUrl {
    const val BASE_URL = "http://ddragon.leagueoflegends.com/"
    const val BASE_WALLPAPERS_URL = "http://screensavers.riotgames.com/"
    const val BASE_URL_WALL = "http://screensavers.riotgames.com/v2/latest/content/"
    const val WALLPAPER_URL = "http://screensavers.riotgames.com/v2/latest/content/data.json"
}