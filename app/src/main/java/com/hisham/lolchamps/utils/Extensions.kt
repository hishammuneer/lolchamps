package com.hisham.lolchamps.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

suspend fun <T> main(
    context: CoroutineContext = Dispatchers.Main,
    block: suspend CoroutineScope.() -> T
): T {
    return withContext(context, block)
}

suspend fun <T> io(
    context: CoroutineContext = Dispatchers.IO,
    block: suspend CoroutineScope.() -> T
): T {
    return withContext(context, block)
}