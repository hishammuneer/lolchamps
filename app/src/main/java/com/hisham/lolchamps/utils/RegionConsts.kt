package com.hisham.lolchamps.utils

/**
 * Created by Hisham on 02/Apr/2017 - 16:17
 */
object RegionConsts {
    // Demacia
    const val demacianStory =
        "<p>Demacia is a strong, lawful society with a prestigious military history. It values the ideals of justice, honor and duty highly, and its people are fiercely proud. Demacia is a self-sufficient, agrarian society, with abundant, fertile farmland, dense forests that are logged for lumber, and mountains rich with mineral resources. It is inherently defensive and insular, partly in response to frequent attacks from barbarians, raiders and expansionist civilizations. Some suggest that the golden age of Demacia has passed and unless it is able to adapt to a changing world – something many believe it is simply incapable of doing – that its decline is inevitable. Nevertheless, Demacia remains one of the dominant powers in Valoran, and boasts the most elite, well-trained army in all of Runeterra.</p>"
    const val demacianCoverImgUrl =
        "http://am-a.akamaihd.net/image?f=http://universe-meeps.leagueoflegends.com/v1/assets/images/factions/demacia_splash.jpg&resize=:600"
}