package com.hisham.lolchamps.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.model.wallpaper.WallPaperModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

class MainActivityViewModel : SplashViewModel() {

    val champList = MutableLiveData<ChampionList?>()
    val images = MutableLiveData<WallPaperModel>()

    fun getChamps(prefs: AppSharedPrefs) {
        viewModelScope.launch {
            val data = withContext(Dispatchers.IO) {
                getChampions(prefs)
            }
            champList.value = data
        }
    }

    fun getWallImages(prefs: AppSharedPrefs) {
        viewModelScope.launch {
            val data = withContext(Dispatchers.IO) {
                getWallpapers(prefs)
            }
            images.value = data
        }
    }

}