package com.hisham.lolchamps.viewmodels

import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.hisham.lolchamps.R
import com.hisham.lolchamps.helper.AppSharedPrefs
import com.hisham.lolchamps.model.wallpaper.WallPaperModel
import com.hisham.lolchamps.retrofit.LolChampsApi
import com.hisham.lolchamps.retrofit.LolRetrofit
import com.hisham.lolchamps.retrofit.LolWallpaperApi
import com.hisham.lolchamps.source.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList

private const val LOADING_WALLS = R.string.loading_all_wall
private const val LOADING_CHAMPS = R.string.loading_champions

open class SplashViewModel : ViewModel() {
    // retrofit services
    private val champService: LolChampsApi = LolRetrofit.champService
    private val wallpaperService: LolWallpaperApi = LolRetrofit.wallpaperService

    // state live data
    val statusLiveData = MutableLiveData<Boolean>()

    // status text live data
    val statusTextLiveData: MutableLiveData<Int> = MutableLiveData()

    fun getLatestVersion(prefs: AppSharedPrefs) { // 4.5
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val dataRepository = DataRepository(champService, wallpaperService, prefs)

                val version = dataRepository.getLatestVersion().data
                if (prefs.dataVersion != version) {
                    // clear all the data
                    prefs.clearAllSharedPreferences()
                }
                // save the version fetched from server into preferences
                prefs.saveStringPreference(AppSharedPrefs.RIOT_DATA_VERSION, version)
                Log.d("Hisham", "version saved in preferences: $version")
                getChampions(prefs)
            }
        }
    }

    @WorkerThread
    suspend fun getChampions(prefs: AppSharedPrefs): ChampionList? {
        statusTextLiveData.postValue(LOADING_CHAMPS)
        // fetching champion list from preferences cache otherwise api
        val dataRepository = DataRepository(champService, wallpaperService, prefs)

        val loLResponse = dataRepository.getChampionsList()
        loLResponse.error?.let {
            statusTextLiveData.postValue(it)
        } ?: run {
            val championsList = loLResponse.data
            championsList?.let {
                prefs.saveStringPreference(AppSharedPrefs.CHAMP_LIST, Gson().toJson(it))
            }
            getWallpapers(prefs)
            return championsList
        }

        return null
    }

    @WorkerThread
    suspend fun getWallpapers(prefs: AppSharedPrefs): WallPaperModel? {
        statusTextLiveData.postValue(LOADING_WALLS)
        val dataRepository = DataRepository(champService, wallpaperService, prefs)
        val wallpaperData = dataRepository.getWallpapers()
        wallpaperData.error?.let {
            statusTextLiveData.postValue(it)
            statusLiveData.postValue(false)
        } ?: run {
            val data = wallpaperData.data
            // saving the wallpaper list to shared preferences
            data?.let{ prefs.saveStringPreference(AppSharedPrefs.WALLPAPER_LIST, Gson().toJson(it)) }
            statusLiveData.postValue(true)
            return data
        }
        return null
    }

}